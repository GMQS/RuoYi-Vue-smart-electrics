package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DeviceInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 配电箱管理Mapper接口
 * 
 * @author yk
 * @date 2024-04-24
 */
@Mapper
public interface DeviceInfoMapper 
{
    /**
     * 查询配电箱管理
     * 
     * @param id 配电箱管理主键
     * @return 配电箱管理
     */
    public DeviceInfo selectDeviceInfoById(String id);

    /**
     * 查询配电箱管理列表
     * 
     * @param deviceInfo 配电箱管理
     * @return 配电箱管理集合
     */
    public List<DeviceInfo> selectDeviceInfoList(DeviceInfo deviceInfo);

    /**
     * 新增配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    public int insertDeviceInfo(DeviceInfo deviceInfo);

    /**
     * 修改配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    public int updateDeviceInfo(DeviceInfo deviceInfo);

    /**
     * 删除配电箱管理
     * 
     * @param id 配电箱管理主键
     * @return 结果
     */
    public int deleteDeviceInfoById(String id);

    /**
     * 批量删除配电箱管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDeviceInfoByIds(String[] ids);
}
