package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CfgShortMsg;

/**
 * msgMapper接口
 * 
 * @author xyq
 * @date 2024-04-29
 */
public interface CfgShortMsgMapper 
{
    /**
     * 查询msg
     * 
     * @param id msg主键
     * @return msg
     */
    public CfgShortMsg selectCfgShortMsgById(Long id);

    /**
     * 查询msg列表
     * 
     * @param cfgShortMsg msg
     * @return msg集合
     */
    public List<CfgShortMsg> selectCfgShortMsgList(CfgShortMsg cfgShortMsg);

    /**
     * 新增msg
     * 
     * @param cfgShortMsg msg
     * @return 结果
     */
    public int insertCfgShortMsg(CfgShortMsg cfgShortMsg);

    /**
     * 修改msg
     * 
     * @param cfgShortMsg msg
     * @return 结果
     */
    public int updateCfgShortMsg(CfgShortMsg cfgShortMsg);

    /**
     * 删除msg
     * 
     * @param id msg主键
     * @return 结果
     */
    public int deleteCfgShortMsgById(Long id);

    /**
     * 批量删除msg
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCfgShortMsgByIds(Long[] ids);
}
