package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsLockFactor;
import org.apache.ibatis.annotations.Mapper;

/**
 * 锁信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Mapper
public interface PdsLockFactorMapper 
{
    /**
     * 查询锁信息
     * 
     * @param id 锁信息主键
     * @return 锁信息
     */
    public PdsLockFactor selectPdsLockFactorById(Long id);

    /**
     * 查询锁信息列表
     * 
     * @param pdsLockFactor 锁信息
     * @return 锁信息集合
     */
    public List<PdsLockFactor> selectPdsLockFactorList(PdsLockFactor pdsLockFactor);

    /**
     * 新增锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    public int insertPdsLockFactor(PdsLockFactor pdsLockFactor);

    /**
     * 修改锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    public int updatePdsLockFactor(PdsLockFactor pdsLockFactor);

    /**
     * 删除锁信息
     * 
     * @param id 锁信息主键
     * @return 结果
     */
    public int deletePdsLockFactorById(Long id);

    /**
     * 批量删除锁信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsLockFactorByIds(Long[] ids);
}
