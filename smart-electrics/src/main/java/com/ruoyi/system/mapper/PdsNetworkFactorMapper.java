package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsNetworkFactor;
import org.apache.ibatis.annotations.Mapper;

/**
 * 网路信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Mapper
public interface PdsNetworkFactorMapper 
{
    /**
     * 查询网路信息
     * 
     * @param id 网路信息主键
     * @return 网路信息
     */
    public PdsNetworkFactor selectPdsNetworkFactorById(Long id);

    /**
     * 查询网路信息列表
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 网路信息集合
     */
    public List<PdsNetworkFactor> selectPdsNetworkFactorList(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 新增网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    public int insertPdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 修改网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    public int updatePdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 删除网路信息
     * 
     * @param id 网路信息主键
     * @return 结果
     */
    public int deletePdsNetworkFactorById(Long id);

    /**
     * 批量删除网路信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsNetworkFactorByIds(Long[] ids);
}
