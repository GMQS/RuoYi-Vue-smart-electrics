package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsAlarmProcess;
import org.apache.ibatis.annotations.Mapper;

/**
 * 告警处理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Mapper
public interface PdsAlarmProcessMapper 
{
    /**
     * 查询告警处理
     * 
     * @param id 告警处理主键
     * @return 告警处理
     */
    public PdsAlarmProcess selectPdsAlarmProcessById(Long id);

    /**
     * 查询告警处理列表
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 告警处理集合
     */
    public List<PdsAlarmProcess> selectPdsAlarmProcessList(PdsAlarmProcess pdsAlarmProcess);

    /**
     * 新增告警处理
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 结果
     */
    public int insertPdsAlarmProcess(PdsAlarmProcess pdsAlarmProcess);

    /**
     * 修改告警处理
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 结果
     */
    public int updatePdsAlarmProcess(PdsAlarmProcess pdsAlarmProcess);

    /**
     * 删除告警处理
     * 
     * @param id 告警处理主键
     * @return 结果
     */
    public int deletePdsAlarmProcessById(Long id);

    /**
     * 批量删除告警处理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsAlarmProcessByIds(Long[] ids);
}
