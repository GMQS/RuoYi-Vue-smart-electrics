package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsMonitorHistory;

/**
 * 历史监控数据Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface PdsMonitorHistoryMapper 
{
    /**
     * 查询历史监控数据
     * 
     * @param id 历史监控数据主键
     * @return 历史监控数据
     */
    public PdsMonitorHistory selectPdsMonitorHistoryById(Long id);

    /**
     * 查询历史监控数据列表
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 历史监控数据集合
     */
    public List<PdsMonitorHistory> selectPdsMonitorHistoryList(PdsMonitorHistory pdsMonitorHistory);

    /**
     * 新增历史监控数据
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 结果
     */
    public int insertPdsMonitorHistory(PdsMonitorHistory pdsMonitorHistory);

    /**
     * 修改历史监控数据
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 结果
     */
    public int updatePdsMonitorHistory(PdsMonitorHistory pdsMonitorHistory);

    /**
     * 删除历史监控数据
     * 
     * @param id 历史监控数据主键
     * @return 结果
     */
    public int deletePdsMonitorHistoryById(Long id);

    /**
     * 批量删除历史监控数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsMonitorHistoryByIds(Long[] ids);
}
