package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsAlarmInfo;

/**
 * 告警信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface PdsAlarmInfoMapper 
{
    /**
     * 查询告警信息
     * 
     * @param id 告警信息主键
     * @return 告警信息
     */
    public PdsAlarmInfo selectPdsAlarmInfoById(Long id);

    /**
     * 查询告警信息列表
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 告警信息集合
     */
    public List<PdsAlarmInfo> selectPdsAlarmInfoList(PdsAlarmInfo pdsAlarmInfo);

    /**
     * 新增告警信息
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 结果
     */
    public int insertPdsAlarmInfo(PdsAlarmInfo pdsAlarmInfo);

    /**
     * 修改告警信息
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 结果
     */
    public int updatePdsAlarmInfo(PdsAlarmInfo pdsAlarmInfo);

    /**
     * 删除告警信息
     * 
     * @param id 告警信息主键
     * @return 结果
     */
    public int deletePdsAlarmInfoById(Long id);

    /**
     * 批量删除告警信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsAlarmInfoByIds(Long[] ids);
}
