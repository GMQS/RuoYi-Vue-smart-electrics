package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PdsBatteryFactor;
import org.apache.ibatis.annotations.Mapper;

/**
 * 电池信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Mapper
public interface PdsBatteryFactorMapper 
{
    /**
     * 查询电池信息
     * 
     * @param id 电池信息主键
     * @return 电池信息
     */
    public PdsBatteryFactor selectPdsBatteryFactorById(Long id);

    /**
     * 查询电池信息列表
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 电池信息集合
     */
    public List<PdsBatteryFactor> selectPdsBatteryFactorList(PdsBatteryFactor pdsBatteryFactor);

    /**
     * 新增电池信息
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 结果
     */
    public int insertPdsBatteryFactor(PdsBatteryFactor pdsBatteryFactor);

    /**
     * 修改电池信息
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 结果
     */
    public int updatePdsBatteryFactor(PdsBatteryFactor pdsBatteryFactor);

    /**
     * 删除电池信息
     * 
     * @param id 电池信息主键
     * @return 结果
     */
    public int deletePdsBatteryFactorById(Long id);

    /**
     * 批量删除电池信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePdsBatteryFactorByIds(Long[] ids);
}
