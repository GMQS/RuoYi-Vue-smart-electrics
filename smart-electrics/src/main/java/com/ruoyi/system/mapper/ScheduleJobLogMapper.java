package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ScheduleJobLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务日志Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
@Mapper
public interface ScheduleJobLogMapper 
{
    /**
     * 查询定时任务日志
     * 
     * @param id 定时任务日志主键
     * @return 定时任务日志
     */
    public ScheduleJobLog selectScheduleJobLogById(Long id);

    /**
     * 查询定时任务日志列表
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 定时任务日志集合
     */
    public List<ScheduleJobLog> selectScheduleJobLogList(ScheduleJobLog scheduleJobLog);

    /**
     * 新增定时任务日志
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 结果
     */
    public int insertScheduleJobLog(ScheduleJobLog scheduleJobLog);

    /**
     * 修改定时任务日志
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 结果
     */
    public int updateScheduleJobLog(ScheduleJobLog scheduleJobLog);

    /**
     * 删除定时任务日志
     * 
     * @param id 定时任务日志主键
     * @return 结果
     */
    public int deleteScheduleJobLogById(Long id);

    /**
     * 批量删除定时任务日志
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScheduleJobLogByIds(Long[] ids);
}
