package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CfgEmail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 告警配置Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@Mapper
public interface CfgEmailMapper 
{
    /**
     * 查询告警配置
     * 
     * @param id 告警配置主键
     * @return 告警配置
     */
    public CfgEmail selectCfgEmailById(Long id);

    /**
     * 查询告警配置列表
     * 
     * @param cfgEmail 告警配置
     * @return 告警配置集合
     */
    public List<CfgEmail> selectCfgEmailList(CfgEmail cfgEmail);

    /**
     * 新增告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    public int insertCfgEmail(CfgEmail cfgEmail);

    /**
     * 修改告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    public int updateCfgEmail(CfgEmail cfgEmail);

    /**
     * 删除告警配置
     * 
     * @param id 告警配置主键
     * @return 结果
     */
    public int deleteCfgEmailById(Long id);

    /**
     * 批量删除告警配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCfgEmailByIds(Long[] ids);
}
