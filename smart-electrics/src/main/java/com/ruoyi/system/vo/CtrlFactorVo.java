package com.ruoyi.system.vo;

public class CtrlFactorVo {

    Long channelId;

    String nodeName;

    String maker;

    String factoryNumber;

    String ctrlType;

    String remark;

    public void setChannelId(Long channelId){ this.channelId = channelId; }
    public Long getChannelId() { return channelId; }

    public void setNodeName(String nodeName)
    {
        this.nodeName = nodeName;
    }
    public String getNodeName()
    {
        return nodeName;
    }

    public void setMaker(String maker)
    {
        this.maker = maker;
    }
    public String getMaker()
    {
        return maker;
    }

    public void setFactoryNumber(String factoryNumber)
    {
        this.factoryNumber = factoryNumber;
    }
    public String getFactoryNumber()
    {
        return factoryNumber;
    }

    public void setCtrlType(String ctrlType)
    {
        this.ctrlType = ctrlType;
    }
    public String getCtrlType()
    {
        return ctrlType;
    }

    public void setRemark(String remark) { this.remark = remark; }
    public String getRemark() { return remark; }
}
