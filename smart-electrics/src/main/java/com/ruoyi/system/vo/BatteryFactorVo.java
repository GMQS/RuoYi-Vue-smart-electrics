package com.ruoyi.system.vo;

public class BatteryFactorVo {

    Long channelId;

    String nodeName;

    String maker;

    String factoryNumber;

    String batteryType;

    String remark;

    public void setChannelId(Long channelId){ this.channelId = channelId; }
    public Long getChannelId() { return channelId; }

    public void setNodeName(String nodeName)
    {
        this.nodeName = nodeName;
    }
    public String getNodeName()
    {
        return nodeName;
    }

    public void setMaker(String maker)
    {
        this.maker = maker;
    }
    public String getMaker()
    {
        return maker;
    }

    public void setFactoryNumber(String factoryNumber)
    {
        this.factoryNumber = factoryNumber;
    }
    public String getFactoryNumber()
    {
        return factoryNumber;
    }

    public void setBatteryType(String batteryType)
    {
        this.batteryType = batteryType;
    }
    public String getBatteryType()
    {
        return batteryType;
    }

    public void setRemark(String remark) { this.remark = remark; }
    public String getRemark() { return remark; }
}
