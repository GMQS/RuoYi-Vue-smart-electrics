package com.ruoyi.system.vo;

public class VolcurFactorVo {

    Long channelId;

    String nodeName;

    String remark;

    public void setChannelId(Long channelId){ this.channelId = channelId; }
    public Long getChannelId() { return channelId; }

    public void setNodeName(String nodeName)
    {
        this.nodeName = nodeName;
    }
    public String getNodeName()
    {
        return nodeName;
    }

    public void setRemark(String remark) { this.remark = remark; }
    public String getRemark() { return remark; }
}
