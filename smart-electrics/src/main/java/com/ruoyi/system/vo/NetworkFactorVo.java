package com.ruoyi.system.vo;

public class NetworkFactorVo {

    Long channelId;

    String nodeName;

    String protocol;

    String ip;

    String remark;

    public void setChannelId(Long channelId){ this.channelId = channelId; }
    public Long getChannelId() { return channelId; }

    public void setNodeName(String nodeName)
    {
        this.nodeName = nodeName;
    }
    public String getNodeName()
    {
        return nodeName;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    public String getProtocol() {
        return protocol;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getIp() {
        return ip;
    }

    public void setRemark(String remark) { this.remark = remark; }
    public String getRemark() { return remark; }
}
