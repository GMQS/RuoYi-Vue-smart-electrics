package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CfgEmail;
import com.ruoyi.system.service.ICfgEmailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 告警配置Controller
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@RestController
@RequestMapping("/data_config/email")
public class CfgEmailController extends BaseController
{
    @Autowired
    private ICfgEmailService cfgEmailService;

    /**
     * 查询告警配置列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:list')")
    @GetMapping("/list")
    public TableDataInfo list(CfgEmail cfgEmail)
    {
        startPage();
        List<CfgEmail> list = cfgEmailService.selectCfgEmailList(cfgEmail);
        return getDataTable(list);
    }

    /**
     * 导出告警配置列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:export')")
    @Log(title = "告警配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CfgEmail cfgEmail)
    {
        List<CfgEmail> list = cfgEmailService.selectCfgEmailList(cfgEmail);
        ExcelUtil<CfgEmail> util = new ExcelUtil<CfgEmail>(CfgEmail.class);
        util.exportExcel(response, list, "告警配置数据");
    }

    /**
     * 获取告警配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cfgEmailService.selectCfgEmailById(id));
    }

    /**
     * 新增告警配置
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:add')")
    @Log(title = "告警配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CfgEmail cfgEmail)
    {
        return toAjax(cfgEmailService.insertCfgEmail(cfgEmail));
    }

    /**
     * 修改告警配置
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:edit')")
    @Log(title = "告警配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CfgEmail cfgEmail)
    {
        return toAjax(cfgEmailService.updateCfgEmail(cfgEmail));
    }

    /**
     * 删除告警配置
     */
    @PreAuthorize("@ss.hasPermi('data_config:email:remove')")
    @Log(title = "告警配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cfgEmailService.deleteCfgEmailByIds(ids));
    }
}
