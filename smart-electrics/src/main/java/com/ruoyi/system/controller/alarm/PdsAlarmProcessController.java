package com.ruoyi.system.controller.alarm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsAlarmProcess;
import com.ruoyi.system.service.IPdsAlarmProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 告警处理Controller
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/alarm/alarm_process")
public class PdsAlarmProcessController extends BaseController
{
    @Autowired
    private IPdsAlarmProcessService pdsAlarmProcessService;

    /**
     * 查询告警处理列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsAlarmProcess pdsAlarmProcess)
    {
        startPage();
        List<PdsAlarmProcess> list = pdsAlarmProcessService.selectPdsAlarmProcessList(pdsAlarmProcess);
        return getDataTable(list);
    }

    /**
     * 导出告警处理列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:export')")
    @Log(title = "告警处理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsAlarmProcess pdsAlarmProcess)
    {
        List<PdsAlarmProcess> list = pdsAlarmProcessService.selectPdsAlarmProcessList(pdsAlarmProcess);
        ExcelUtil<PdsAlarmProcess> util = new ExcelUtil<PdsAlarmProcess>(PdsAlarmProcess.class);
        util.exportExcel(response, list, "告警处理数据");
    }

    /**
     * 获取告警处理详细信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsAlarmProcessService.selectPdsAlarmProcessById(id));
    }

    /**
     * 新增告警处理
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:add')")
    @Log(title = "告警处理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsAlarmProcess pdsAlarmProcess)
    {
        return toAjax(pdsAlarmProcessService.insertPdsAlarmProcess(pdsAlarmProcess));
    }

    /**
     * 修改告警处理
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:edit')")
    @Log(title = "告警处理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsAlarmProcess pdsAlarmProcess)
    {
        return toAjax(pdsAlarmProcessService.updatePdsAlarmProcess(pdsAlarmProcess));
    }

    /**
     * 删除告警处理
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_process:remove')")
    @Log(title = "告警处理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsAlarmProcessService.deletePdsAlarmProcessByIds(ids));
    }
}
