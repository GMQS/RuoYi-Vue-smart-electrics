package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ScheduleJobLog;
import com.ruoyi.system.service.IScheduleJobLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 定时任务日志Controller
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/data_config/timing")
public class ScheduleJobLogController extends BaseController
{
    @Autowired
    private IScheduleJobLogService scheduleJobLogService;

    /**
     * 查询定时任务日志列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScheduleJobLog scheduleJobLog)
    {
        startPage();
        List<ScheduleJobLog> list = scheduleJobLogService.selectScheduleJobLogList(scheduleJobLog);
        return getDataTable(list);
    }

    /**
     * 导出定时任务日志列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:export')")
    @Log(title = "定时任务日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScheduleJobLog scheduleJobLog)
    {
        List<ScheduleJobLog> list = scheduleJobLogService.selectScheduleJobLogList(scheduleJobLog);
        ExcelUtil<ScheduleJobLog> util = new ExcelUtil<ScheduleJobLog>(ScheduleJobLog.class);
        util.exportExcel(response, list, "定时任务日志数据");
    }

    /**
     * 获取定时任务日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scheduleJobLogService.selectScheduleJobLogById(id));
    }

    /**
     * 新增定时任务日志
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:add')")
    @Log(title = "定时任务日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScheduleJobLog scheduleJobLog)
    {
        return toAjax(scheduleJobLogService.insertScheduleJobLog(scheduleJobLog));
    }

    /**
     * 修改定时任务日志
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:edit')")
    @Log(title = "定时任务日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScheduleJobLog scheduleJobLog)
    {
        return toAjax(scheduleJobLogService.updateScheduleJobLog(scheduleJobLog));
    }

    /**
     * 删除定时任务日志
     */
    @PreAuthorize("@ss.hasPermi('data_config:timing:remove')")
    @Log(title = "定时任务日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scheduleJobLogService.deleteScheduleJobLogByIds(ids));
    }
}
