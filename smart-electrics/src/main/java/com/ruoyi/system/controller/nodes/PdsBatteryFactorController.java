package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.system.vo.BatteryFactorVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsBatteryFactor;
import com.ruoyi.system.service.IPdsBatteryFactorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电池信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/battery_factor")
public class PdsBatteryFactorController extends BaseController
{
    @Autowired
    private IPdsBatteryFactorService pdsBatteryFactorService;

    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询电池信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsBatteryFactor pdsBatteryFactor)
    {
        startPage();
        List<PdsBatteryFactor> list = pdsBatteryFactorService.selectPdsBatteryFactorList(pdsBatteryFactor);
        return getDataTable(list);
    }

    /**
     * 导出电池信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:export')")
    @Log(title = "电池信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsBatteryFactor pdsBatteryFactor)
    {
        List<PdsBatteryFactor> list = pdsBatteryFactorService.selectPdsBatteryFactorList(pdsBatteryFactor);
        ExcelUtil<PdsBatteryFactor> util = new ExcelUtil<PdsBatteryFactor>(PdsBatteryFactor.class);
        util.exportExcel(response, list, "电池信息数据");
    }

    /**
     * 获取电池信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsBatteryFactorService.selectPdsBatteryFactorById(id));
    }

    /**
     * 新增电池信息
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:add')")
    @Log(title = "电池信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BatteryFactorVo batteryFactorVo)
    {
        PdsBatteryFactor pdsBatteryFactor = new PdsBatteryFactor();
        pdsBatteryFactor.setNodeName(batteryFactorVo.getNodeName());
        pdsBatteryFactor.setMaker(batteryFactorVo.getMaker());
        pdsBatteryFactor.setFactoryNumber(batteryFactorVo.getFactoryNumber());
        pdsBatteryFactor.setBatteryType(batteryFactorVo.getBatteryType());
        pdsBatteryFactor.setRemark(batteryFactorVo.getRemark());
        // 默认值
        pdsBatteryFactor.setDeviceEnable(true);
        pdsBatteryFactor.setMonitorEnable(true);
        pdsBatteryFactor.setMonitorInterval(new Long(6));
        pdsBatteryFactor.setReportInterval(new Long(6));
        pdsBatteryFactor.setAlarmLowerLimit(new Long(6));
        pdsBatteryFactor.setAlarmInterval(new Long(6));
        pdsBatteryFactor.setAlarmDelay(new Long(6));
        pdsBatteryFactor.setOfflineAlarm(true);
        pdsBatteryFactor.setAlarmMsg(true);
        pdsBatteryFactor.setAlarmEmail(true);
        pdsBatteryFactorService.insertPdsBatteryFactor(pdsBatteryFactor);

        PdsChannelNode pdsChannelNode = new PdsChannelNode();
        pdsChannelNode.setChannelId(batteryFactorVo.getChannelId());
        pdsChannelNode.setNodeType("1");
        pdsChannelNode.setNodeId(pdsBatteryFactor.getId());
        int ret = pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode);

        return  toAjax(ret);
    }

    /**
     * 修改电池信息
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:edit')")
    @Log(title = "电池信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsBatteryFactor pdsBatteryFactor)
    {
        return toAjax(pdsBatteryFactorService.updatePdsBatteryFactor(pdsBatteryFactor));
    }

    /**
     * 删除电池信息
     */
    @PreAuthorize("@ss.hasPermi('devices:battery_factor:remove')")
    @Log(title = "电池信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestBody PdsChannelNode pdsChannelNode)
    {
        pdsChannelNodeService.deleteByChannelIdAndNodeId(pdsChannelNode);
        int ret = pdsBatteryFactorService.deletePdsBatteryFactorById(pdsChannelNode.getNodeId());
        return toAjax(ret);
    }
}
