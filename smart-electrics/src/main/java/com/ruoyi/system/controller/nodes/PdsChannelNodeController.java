package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 管道节点关联Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/channel_node")
public class PdsChannelNodeController extends BaseController
{
    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询管道节点关联列表
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsChannelNode pdsChannelNode)
    {
        startPage();
        List<PdsChannelNode> list = pdsChannelNodeService.selectPdsChannelNodeList(pdsChannelNode);
        return getDataTable(list);
    }

    /**
     * 查询管道下所有节点
     *{ id: 5, pipelineId: 2, name: '电压电流节点管理' },
     * 电池节点管理,电控节点管理,锁节点管理,网路节点管理,电压电流节点管理
     * @param channelId 管道id
     * @return 管道节点关联集合
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:query')")
    @GetMapping(value = "/nodes/{channelId}")
    public AjaxResult selectPdsChannelNodeListByChannelId(@PathVariable("channelId") Long channelId){
        List<PdsChannelNode> channelNodeList = pdsChannelNodeService.selectPdsChannelNodeListByChannelId(channelId);
        JSONArray ret = new JSONArray();
        channelNodeList.forEach(channelNode->{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", channelNode.getNodeId());
            jsonObject.put("pipelineId", channelNode.getChannelId());
            String nodeType = channelNode.getNodeType();
            if(nodeType.equals("1")) jsonObject.put("name", "电池节点管理");
            else if(nodeType.equals("2")) jsonObject.put("name", "电控节点管理");
            else if(nodeType.equals("3")) jsonObject.put("name", "锁节点管理");
            else if(nodeType.equals("4")) jsonObject.put("name", "网路节点管理");
            else if(nodeType.equals("5")) jsonObject.put("name", "电压电流节点管理");
            ret.add(jsonObject);
        });
        return success(ret);
    };

    /**
     * 导出管道节点关联列表
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:export')")
    @Log(title = "管道节点关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsChannelNode pdsChannelNode)
    {
        List<PdsChannelNode> list = pdsChannelNodeService.selectPdsChannelNodeList(pdsChannelNode);
        ExcelUtil<PdsChannelNode> util = new ExcelUtil<PdsChannelNode>(PdsChannelNode.class);
        util.exportExcel(response, list, "管道节点关联数据");
    }

    /**
     * 获取管道节点关联详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsChannelNodeService.selectPdsChannelNodeById(id));
    }

    /**
     * 新增管道节点关联
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:add')")
    @Log(title = "管道节点关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsChannelNode pdsChannelNode)
    {
        return toAjax(pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode));
    }

    /**
     * 修改管道节点关联
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:edit')")
    @Log(title = "管道节点关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsChannelNode pdsChannelNode)
    {
        return toAjax(pdsChannelNodeService.updatePdsChannelNode(pdsChannelNode));
    }

    /**
     * 删除管道节点关联
     */
    @PreAuthorize("@ss.hasPermi('devices:channel_node:remove')")
    @Log(title = "管道节点关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsChannelNodeService.deletePdsChannelNodeByIds(ids));
    }
}
