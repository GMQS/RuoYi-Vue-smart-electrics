package com.ruoyi.system.controller.monitor;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsMonitorReal;
import com.ruoyi.system.service.IPdsMonitorRealService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实时监控数据Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/monitor/monitor_real")
public class PdsMonitorRealController extends BaseController
{
    @Autowired
    private IPdsMonitorRealService pdsMonitorRealService;

    /**
     * 查询实时监控数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsMonitorReal pdsMonitorReal)
    {
        startPage();
        List<PdsMonitorReal> list = pdsMonitorRealService.selectPdsMonitorRealList(pdsMonitorReal);
        return getDataTable(list);
    }

    /**
     * 导出实时监控数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:export')")
    @Log(title = "实时监控数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsMonitorReal pdsMonitorReal)
    {
        List<PdsMonitorReal> list = pdsMonitorRealService.selectPdsMonitorRealList(pdsMonitorReal);
        ExcelUtil<PdsMonitorReal> util = new ExcelUtil<PdsMonitorReal>(PdsMonitorReal.class);
        util.exportExcel(response, list, "实时监控数据数据");
    }

    /**
     * 获取实时监控数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsMonitorRealService.selectPdsMonitorRealById(id));
    }

    /**
     * 新增实时监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:add')")
    @Log(title = "实时监控数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsMonitorReal pdsMonitorReal)
    {
        return toAjax(pdsMonitorRealService.insertPdsMonitorReal(pdsMonitorReal));
    }

    /**
     * 修改实时监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:edit')")
    @Log(title = "实时监控数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsMonitorReal pdsMonitorReal)
    {
        return toAjax(pdsMonitorRealService.updatePdsMonitorReal(pdsMonitorReal));
    }

    /**
     * 删除实时监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_real:remove')")
    @Log(title = "实时监控数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsMonitorRealService.deletePdsMonitorRealByIds(ids));
    }
}
