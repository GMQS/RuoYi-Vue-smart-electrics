package com.ruoyi.system.controller.alarm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsAllAlarmInfo;
import com.ruoyi.system.service.IPdsAllAlarmInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 详细告警记录Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/alarm/all_alarm_info")
public class PdsAllAlarmInfoController extends BaseController
{
    @Autowired
    private IPdsAllAlarmInfoService pdsAllAlarmInfoService;

    /**
     * 查询详细告警记录列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        startPage();
        List<PdsAllAlarmInfo> list = pdsAllAlarmInfoService.selectPdsAllAlarmInfoList(pdsAllAlarmInfo);
        return getDataTable(list);
    }

    /**
     * 导出详细告警记录列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:export')")
    @Log(title = "详细告警记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        List<PdsAllAlarmInfo> list = pdsAllAlarmInfoService.selectPdsAllAlarmInfoList(pdsAllAlarmInfo);
        ExcelUtil<PdsAllAlarmInfo> util = new ExcelUtil<PdsAllAlarmInfo>(PdsAllAlarmInfo.class);
        util.exportExcel(response, list, "详细告警记录数据");
    }

    /**
     * 获取详细告警记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsAllAlarmInfoService.selectPdsAllAlarmInfoById(id));
    }

    /**
     * 新增详细告警记录
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:add')")
    @Log(title = "详细告警记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        return toAjax(pdsAllAlarmInfoService.insertPdsAllAlarmInfo(pdsAllAlarmInfo));
    }

    /**
     * 修改详细告警记录
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:edit')")
    @Log(title = "详细告警记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        return toAjax(pdsAllAlarmInfoService.updatePdsAllAlarmInfo(pdsAllAlarmInfo));
    }

    /**
     * 删除详细告警记录
     */
    @PreAuthorize("@ss.hasPermi('alarm:all_alarm_info:remove')")
    @Log(title = "详细告警记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsAllAlarmInfoService.deletePdsAllAlarmInfoByIds(ids));
    }
}
