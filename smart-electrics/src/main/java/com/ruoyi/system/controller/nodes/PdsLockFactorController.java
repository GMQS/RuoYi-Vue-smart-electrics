package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.domain.PdsCtrlFactor;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.system.vo.LockFactorVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsLockFactor;
import com.ruoyi.system.service.IPdsLockFactorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 锁信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/lock_factor")
public class PdsLockFactorController extends BaseController
{
    @Autowired
    private IPdsLockFactorService pdsLockFactorService;

    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询锁信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsLockFactor pdsLockFactor)
    {
        startPage();
        List<PdsLockFactor> list = pdsLockFactorService.selectPdsLockFactorList(pdsLockFactor);
        return getDataTable(list);
    }

    /**
     * 导出锁信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:export')")
    @Log(title = "锁信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsLockFactor pdsLockFactor)
    {
        List<PdsLockFactor> list = pdsLockFactorService.selectPdsLockFactorList(pdsLockFactor);
        ExcelUtil<PdsLockFactor> util = new ExcelUtil<PdsLockFactor>(PdsLockFactor.class);
        util.exportExcel(response, list, "锁信息数据");
    }

    /**
     * 获取锁信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsLockFactorService.selectPdsLockFactorById(id));
    }

    /**
     * 新增锁信息
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:add')")
    @Log(title = "锁信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LockFactorVo lockFactorVo)
    {
        PdsLockFactor pdsLockFactor = new PdsLockFactor();
        pdsLockFactor.setNodeName(lockFactorVo.getNodeName());
        pdsLockFactor.setMaker(lockFactorVo.getMaker());
        pdsLockFactor.setFactoryNumber(lockFactorVo.getFactoryNumber());
        pdsLockFactor.setLockType(lockFactorVo.getLockType());
        pdsLockFactor.setRemark(lockFactorVo.getRemark());
        // 默认值
        pdsLockFactor.setDeviceEnable(true);
        pdsLockFactor.setMonitorEnable(true);
        pdsLockFactor.setMonitorInterval(new Long(6));
        pdsLockFactor.setReportInterval(new Long(6));
        pdsLockFactor.setFailUpperLimit(new Long(6));
        pdsLockFactor.setAlarmInterval(new Long(6));
        pdsLockFactor.setAlarmDelay(new Long(6));
        pdsLockFactor.setOfflineAlarm(true);
        pdsLockFactor.setAlarmMsg(true);
        pdsLockFactor.setAlarmEmail(true);
        pdsLockFactorService.insertPdsLockFactor(pdsLockFactor);

        PdsChannelNode pdsChannelNode = new PdsChannelNode();
        pdsChannelNode.setChannelId(lockFactorVo.getChannelId());
        pdsChannelNode.setNodeType("3");
        pdsChannelNode.setNodeId(pdsLockFactor.getId());
        int ret = pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode);

        return toAjax(ret);
    }

    /**
     * 修改锁信息
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:edit')")
    @Log(title = "锁信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsLockFactor pdsLockFactor)
    {
        return toAjax(pdsLockFactorService.updatePdsLockFactor(pdsLockFactor));
    }

    /**
     * 删除锁信息
     */
    @PreAuthorize("@ss.hasPermi('devices:lock_factor:remove')")
    @Log(title = "锁信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestBody PdsChannelNode pdsChannelNode)
    {
        pdsChannelNodeService.deleteByChannelIdAndNodeId(pdsChannelNode);
        int ret = pdsLockFactorService.deletePdsLockFactorById(pdsChannelNode.getNodeId());
        return toAjax(ret);
    }
}
