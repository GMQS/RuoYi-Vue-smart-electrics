package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DeviceInfo;
import com.ruoyi.system.service.IDeviceInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 配电箱管理Controller
 * 
 * @author yk
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/devices/device_info")
public class DeviceInfoController extends BaseController
{
    @Autowired
    private IDeviceInfoService deviceInfoService;

    /**
     * 查询配电箱管理列表
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:list')")
    @GetMapping("/list")
    public TableDataInfo list(DeviceInfo deviceInfo)
    {
        startPage();
        List<DeviceInfo> list = deviceInfoService.selectDeviceInfoList(deviceInfo);
        return getDataTable(list);
    }

    /**
     * 导出配电箱管理列表
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:export')")
    @Log(title = "配电箱管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DeviceInfo deviceInfo)
    {
        List<DeviceInfo> list = deviceInfoService.selectDeviceInfoList(deviceInfo);
        ExcelUtil<DeviceInfo> util = new ExcelUtil<DeviceInfo>(DeviceInfo.class);
        util.exportExcel(response, list, "配电箱管理数据");
    }

    /**
     * 获取配电箱管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(deviceInfoService.selectDeviceInfoById(id));
    }

    /**
     * 新增配电箱管理
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:add')")
    @Log(title = "配电箱管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DeviceInfo deviceInfo)
    {

        return toAjax(deviceInfoService.insertDeviceInfo(deviceInfo));
    }

    /**
     * 修改配电箱管理
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:edit')")
    @Log(title = "配电箱管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DeviceInfo deviceInfo)
    {
        System.out.println("--------"+deviceInfo.getAreaName()+"-------------");
        System.out.println("--------"+deviceInfo.getProjectName()+"-------------");
        return toAjax(deviceInfoService.updateDeviceInfo(deviceInfo));
    }

    /**
     * 删除配电箱管理
     */
    @PreAuthorize("@ss.hasPermi('devices:device_info:remove')")
    @Log(title = "配电箱管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(deviceInfoService.deleteDeviceInfoByIds(ids));
    }
}
