package com.ruoyi.system.controller.alarm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsAlarmOperLog;
import com.ruoyi.system.service.IPdsAlarmOperLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 告警操作日志Controller
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/log/alarm_oper_log")
public class PdsAlarmOperLogController extends BaseController
{
    @Autowired
    private IPdsAlarmOperLogService pdsAlarmOperLogService;

    /**
     * 查询告警操作日志列表
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsAlarmOperLog pdsAlarmOperLog)
    {
        startPage();
        List<PdsAlarmOperLog> list = pdsAlarmOperLogService.selectPdsAlarmOperLogList(pdsAlarmOperLog);
        return getDataTable(list);
    }

    /**
     * 导出告警操作日志列表
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:export')")
    @Log(title = "告警操作日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsAlarmOperLog pdsAlarmOperLog)
    {
        List<PdsAlarmOperLog> list = pdsAlarmOperLogService.selectPdsAlarmOperLogList(pdsAlarmOperLog);
        ExcelUtil<PdsAlarmOperLog> util = new ExcelUtil<PdsAlarmOperLog>(PdsAlarmOperLog.class);
        util.exportExcel(response, list, "告警操作日志数据");
    }

    /**
     * 获取告警操作日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsAlarmOperLogService.selectPdsAlarmOperLogById(id));
    }

    /**
     * 新增告警操作日志
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:add')")
    @Log(title = "告警操作日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsAlarmOperLog pdsAlarmOperLog)
    {
        return toAjax(pdsAlarmOperLogService.insertPdsAlarmOperLog(pdsAlarmOperLog));
    }

    /**
     * 修改告警操作日志
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:edit')")
    @Log(title = "告警操作日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsAlarmOperLog pdsAlarmOperLog)
    {
        return toAjax(pdsAlarmOperLogService.updatePdsAlarmOperLog(pdsAlarmOperLog));
    }

    /**
     * 删除告警操作日志
     */
    @PreAuthorize("@ss.hasPermi('log:alarm_oper_log:remove')")
    @Log(title = "告警操作日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsAlarmOperLogService.deletePdsAlarmOperLogByIds(ids));
    }
}
