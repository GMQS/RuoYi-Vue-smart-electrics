package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusGetway;
import com.ruoyi.system.service.IBusGetwayService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 网关
Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/bus_getway")
public class BusGetwayController extends BaseController
{
    @Autowired
    private IBusGetwayService busGetwayService;

    /**
     * 查询网关
列表
     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusGetway busGetway)
    {
        startPage();
        List<BusGetway> list = busGetwayService.selectBusGetwayList(busGetway);
        return getDataTable(list);
    }

    /**
     * 导出网关列表
     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:export')")
    @Log(title = "网关", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusGetway busGetway)
    {
        List<BusGetway> list = busGetwayService.selectBusGetwayList(busGetway);
        ExcelUtil<BusGetway> util = new ExcelUtil<BusGetway>(BusGetway.class);
        util.exportExcel(response, list, "网关数据");
    }

    /**
     * 获取网关
详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(busGetwayService.selectBusGetwayById(id));
    }

    /**
     * 新增网关

     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:add')")
    @Log(title = "网关", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusGetway busGetway)
    {
        return toAjax(busGetwayService.insertBusGetway(busGetway));
    }

    /**
     * 修改网关

     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:edit')")
    @Log(title = "网关", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusGetway busGetway)
    {
        return toAjax(busGetwayService.updateBusGetway(busGetway));
    }

    /**
     * 删除网关

     */
    @PreAuthorize("@ss.hasPermi('devices:bus_getway:remove')")
    @Log(title = "网关", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busGetwayService.deleteBusGetwayByIds(ids));
    }
}
