package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.system.vo.CtrlFactorVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsCtrlFactor;
import com.ruoyi.system.service.IPdsCtrlFactorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电控信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/ctrl_factor")
public class PdsCtrlFactorController extends BaseController
{
    @Autowired
    private IPdsCtrlFactorService pdsCtrlFactorService;

    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询电控信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsCtrlFactor pdsCtrlFactor)
    {
        startPage();
        List<PdsCtrlFactor> list = pdsCtrlFactorService.selectPdsCtrlFactorList(pdsCtrlFactor);
        return getDataTable(list);
    }

    /**
     * 导出电控信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:export')")
    @Log(title = "电控信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsCtrlFactor pdsCtrlFactor)
    {
        List<PdsCtrlFactor> list = pdsCtrlFactorService.selectPdsCtrlFactorList(pdsCtrlFactor);
        ExcelUtil<PdsCtrlFactor> util = new ExcelUtil<PdsCtrlFactor>(PdsCtrlFactor.class);
        util.exportExcel(response, list, "电控信息数据");
    }

    /**
     * 获取电控信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsCtrlFactorService.selectPdsCtrlFactorById(id));
    }

    /**
     * 新增电控信息
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:add')")
    @Log(title = "电控信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CtrlFactorVo ctrlFactorVo)
    {
        PdsCtrlFactor pdsCtrlFactor = new PdsCtrlFactor();
        pdsCtrlFactor.setNodeName(ctrlFactorVo.getNodeName());
        pdsCtrlFactor.setMaker(ctrlFactorVo.getMaker());
        pdsCtrlFactor.setFactoryNumber(ctrlFactorVo.getFactoryNumber());
        pdsCtrlFactor.setCtrlType(ctrlFactorVo.getCtrlType());
        pdsCtrlFactor.setRemark(ctrlFactorVo.getRemark());
        // 默认值
        pdsCtrlFactor.setDeviceEnable(true);
        pdsCtrlFactor.setMonitorEnable(true);
        pdsCtrlFactor.setPowerSavingMode(true);
        pdsCtrlFactor.setMonitorInterval(new Long(6));
        pdsCtrlFactor.setReportInterval(new Long(6));
        pdsCtrlFactor.setAlarmInterval(new Long(6));
        pdsCtrlFactor.setAlarmDelay(new Long(6));
        pdsCtrlFactor.setOfflineAlarm(true);
        pdsCtrlFactor.setAlarmMsg(true);
        pdsCtrlFactor.setAlarmEmail(true);
        pdsCtrlFactorService.insertPdsCtrlFactor(pdsCtrlFactor);

        PdsChannelNode pdsChannelNode = new PdsChannelNode();
        pdsChannelNode.setChannelId(ctrlFactorVo.getChannelId());
        pdsChannelNode.setNodeType("2");
        pdsChannelNode.setNodeId(pdsCtrlFactor.getId());
        int ret = pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode);

        return toAjax(ret);
    }

    /**
     * 修改电控信息
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:edit')")
    @Log(title = "电控信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsCtrlFactor pdsCtrlFactor)
    {
        return toAjax(pdsCtrlFactorService.updatePdsCtrlFactor(pdsCtrlFactor));
    }

    /**
     * 删除电池信息
     */
    @PreAuthorize("@ss.hasPermi('devices:ctrl_factor:remove')")
    @Log(title = "电池信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestBody PdsChannelNode pdsChannelNode)
    {
        pdsChannelNodeService.deleteByChannelIdAndNodeId(pdsChannelNode);
        int ret = pdsCtrlFactorService.deletePdsCtrlFactorById(pdsChannelNode.getNodeId());
        return toAjax(ret);
    }
}
