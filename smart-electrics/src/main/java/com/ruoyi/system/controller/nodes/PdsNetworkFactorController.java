package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.system.vo.NetworkFactorVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsNetworkFactor;
import com.ruoyi.system.service.IPdsNetworkFactorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 网路信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/network_factor")
public class PdsNetworkFactorController extends BaseController
{
    @Autowired
    private IPdsNetworkFactorService pdsNetworkFactorService;

    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询网路信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsNetworkFactor pdsNetworkFactor)
    {
        startPage();
        List<PdsNetworkFactor> list = pdsNetworkFactorService.selectPdsNetworkFactorList(pdsNetworkFactor);
        return getDataTable(list);
    }

    /**
     * 导出网路信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:export')")
    @Log(title = "网路信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsNetworkFactor pdsNetworkFactor)
    {
        List<PdsNetworkFactor> list = pdsNetworkFactorService.selectPdsNetworkFactorList(pdsNetworkFactor);
        ExcelUtil<PdsNetworkFactor> util = new ExcelUtil<PdsNetworkFactor>(PdsNetworkFactor.class);
        util.exportExcel(response, list, "网路信息数据");
    }

    /**
     * 获取网路信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsNetworkFactorService.selectPdsNetworkFactorById(id));
    }

    /**
     * 新增网路信息
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:add')")
    @Log(title = "网路信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NetworkFactorVo networkFactorVo)
    {
        PdsNetworkFactor pdsNetworkFactor = new PdsNetworkFactor();
        pdsNetworkFactor.setNodeName(networkFactorVo.getNodeName());
        pdsNetworkFactor.setProtocol(networkFactorVo.getProtocol());
        pdsNetworkFactor.setIp(networkFactorVo.getIp());
        pdsNetworkFactor.setRemark(networkFactorVo.getRemark());
        // 默认值
        pdsNetworkFactor.setPerMonitor(true);
        pdsNetworkFactor.setNetflowMonitor(true);
        pdsNetworkFactor.setMonitorInterval(new Long(6));
        pdsNetworkFactor.setReportInterval(new Long(6));
        pdsNetworkFactor.setPerUpperLimit(new Long(6));
        pdsNetworkFactor.setAlarmInterval(new Long(6));
        pdsNetworkFactor.setAlarmDelay(new Long(6));
        pdsNetworkFactor.setAlarmMsg(true);
        pdsNetworkFactor.setAlarmEmail(true);
        pdsNetworkFactorService.insertPdsNetworkFactor(pdsNetworkFactor);

        PdsChannelNode pdsChannelNode = new PdsChannelNode();
        pdsChannelNode.setChannelId(networkFactorVo.getChannelId());
        pdsChannelNode.setNodeType("4");
        pdsChannelNode.setNodeId(pdsNetworkFactor.getId());
        int ret = pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode);

        return toAjax(ret);
    }

    /**
     * 修改网路信息
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:edit')")
    @Log(title = "网路信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsNetworkFactor pdsNetworkFactor)
    {
        return toAjax(pdsNetworkFactorService.updatePdsNetworkFactor(pdsNetworkFactor));
    }

    /**
     * 删除网路信息
     */
    @PreAuthorize("@ss.hasPermi('devices:network_factor:remove')")
    @Log(title = "网路信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestBody PdsChannelNode pdsChannelNode)
    {
        pdsChannelNodeService.deleteByChannelIdAndNodeId(pdsChannelNode);
        int ret = pdsNetworkFactorService.deletePdsNetworkFactorById(pdsChannelNode.getNodeId());
        return toAjax(ret);
    }
}
