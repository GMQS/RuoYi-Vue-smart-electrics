package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusOperLog;
import com.ruoyi.system.service.IBusOperLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * oper_logController
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/log/oper_log")
public class BusOperLogController extends BaseController
{
    @Autowired
    private IBusOperLogService busOperLogService;

    /**
     * 查询oper_log列表
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusOperLog busOperLog)
    {
        startPage();
        List<BusOperLog> list = busOperLogService.selectBusOperLogList(busOperLog);
        return getDataTable(list);
    }

    /**
     * 导出oper_log列表
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:export')")
    @Log(title = "oper_log", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusOperLog busOperLog)
    {
        List<BusOperLog> list = busOperLogService.selectBusOperLogList(busOperLog);
        ExcelUtil<BusOperLog> util = new ExcelUtil<BusOperLog>(BusOperLog.class);
        util.exportExcel(response, list, "oper_log数据");
    }

    /**
     * 获取oper_log详细信息
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busOperLogService.selectBusOperLogById(id));
    }

    /**
     * 新增oper_log
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:add')")
    @Log(title = "oper_log", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusOperLog busOperLog)
    {
        return toAjax(busOperLogService.insertBusOperLog(busOperLog));
    }

    /**
     * 修改oper_log
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:edit')")
    @Log(title = "oper_log", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusOperLog busOperLog)
    {
        return toAjax(busOperLogService.updateBusOperLog(busOperLog));
    }

    /**
     * 删除oper_log
     */
    @PreAuthorize("@ss.hasPermi('log:oper_log:remove')")
    @Log(title = "oper_log", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busOperLogService.deleteBusOperLogByIds(ids));
    }
}
