package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CfgShortMsg;
import com.ruoyi.system.service.ICfgShortMsgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * msgController
 * 
 * @author xyq
 * @date 2024-04-29
 */
@RestController
@RequestMapping("/data_config/msg")
public class CfgShortMsgController extends BaseController
{
    @Autowired
    private ICfgShortMsgService cfgShortMsgService;

    /**
     * 查询msg列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:list')")
    @GetMapping("/list")
    public TableDataInfo list(CfgShortMsg cfgShortMsg)
    {
        startPage();
        List<CfgShortMsg> list = cfgShortMsgService.selectCfgShortMsgList(cfgShortMsg);
        return getDataTable(list);
    }

    /**
     * 导出msg列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:export')")
    @Log(title = "msg", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CfgShortMsg cfgShortMsg)
    {
        List<CfgShortMsg> list = cfgShortMsgService.selectCfgShortMsgList(cfgShortMsg);
        ExcelUtil<CfgShortMsg> util = new ExcelUtil<CfgShortMsg>(CfgShortMsg.class);
        util.exportExcel(response, list, "msg数据");
    }

    /**
     * 获取msg详细信息
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cfgShortMsgService.selectCfgShortMsgById(id));
    }

    /**
     * 新增msg
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:add')")
    @Log(title = "msg", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CfgShortMsg cfgShortMsg)
    {
        return toAjax(cfgShortMsgService.insertCfgShortMsg(cfgShortMsg));
    }

    /**
     * 修改msg
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:edit')")
    @Log(title = "msg", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CfgShortMsg cfgShortMsg)
    {
        return toAjax(cfgShortMsgService.updateCfgShortMsg(cfgShortMsg));
    }

    /**
     * 删除msg
     */
    @PreAuthorize("@ss.hasPermi('data_config:msg:remove')")
    @Log(title = "msg", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cfgShortMsgService.deleteCfgShortMsgByIds(ids));
    }
}
