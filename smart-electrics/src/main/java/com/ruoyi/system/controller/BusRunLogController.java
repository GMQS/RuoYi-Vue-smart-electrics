package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusRunLog;
import com.ruoyi.system.service.IBusRunLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * run_logController
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/log/run_log")
public class BusRunLogController extends BaseController
{
    @Autowired
    private IBusRunLogService busRunLogService;

    /**
     * 查询run_log列表
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusRunLog busRunLog)
    {
        startPage();
        List<BusRunLog> list = busRunLogService.selectBusRunLogList(busRunLog);
        return getDataTable(list);
    }

    /**
     * 导出run_log列表
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:export')")
    @Log(title = "run_log", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusRunLog busRunLog)
    {
        List<BusRunLog> list = busRunLogService.selectBusRunLogList(busRunLog);
        ExcelUtil<BusRunLog> util = new ExcelUtil<BusRunLog>(BusRunLog.class);
        util.exportExcel(response, list, "run_log数据");
    }

    /**
     * 获取run_log详细信息
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busRunLogService.selectBusRunLogById(id));
    }

    /**
     * 新增run_log
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:add')")
    @Log(title = "run_log", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusRunLog busRunLog)
    {
        return toAjax(busRunLogService.insertBusRunLog(busRunLog));
    }

    /**
     * 修改run_log
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:edit')")
    @Log(title = "run_log", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusRunLog busRunLog)
    {
        return toAjax(busRunLogService.updateBusRunLog(busRunLog));
    }

    /**
     * 删除run_log
     */
    @PreAuthorize("@ss.hasPermi('log:run_log:remove')")
    @Log(title = "run_log", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busRunLogService.deleteBusRunLogByIds(ids));
    }
}
