package com.ruoyi.system.controller.nodes;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;
import com.ruoyi.system.vo.VolcurFactorVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsVolcurFactor;
import com.ruoyi.system.service.IPdsVolcurFactorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电压电流信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/devices/volcur_factor")
public class PdsVolcurFactorController extends BaseController
{
    @Autowired
    private IPdsVolcurFactorService pdsVolcurFactorService;

    @Autowired
    private IPdsChannelNodeService pdsChannelNodeService;

    /**
     * 查询电压电流信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsVolcurFactor pdsVolcurFactor)
    {
        startPage();
        List<PdsVolcurFactor> list = pdsVolcurFactorService.selectPdsVolcurFactorList(pdsVolcurFactor);
        return getDataTable(list);
    }

    /**
     * 导出电压电流信息列表
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:export')")
    @Log(title = "电压电流信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsVolcurFactor pdsVolcurFactor)
    {
        List<PdsVolcurFactor> list = pdsVolcurFactorService.selectPdsVolcurFactorList(pdsVolcurFactor);
        ExcelUtil<PdsVolcurFactor> util = new ExcelUtil<PdsVolcurFactor>(PdsVolcurFactor.class);
        util.exportExcel(response, list, "电压电流信息数据");
    }

    /**
     * 获取电压电流信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsVolcurFactorService.selectPdsVolcurFactorById(id));
    }

    /**
     * 新增电压电流信息
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:add')")
    @Log(title = "电压电流信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VolcurFactorVo volcurFactorVo)
    {
        PdsVolcurFactor pdsVolcurFactor = new PdsVolcurFactor();
        pdsVolcurFactor.setNodeName(volcurFactorVo.getNodeName());
        pdsVolcurFactor.setRemark(volcurFactorVo.getRemark());
        // 默认值
        pdsVolcurFactor.setVolMonEnable(true);
        pdsVolcurFactor.setCurMonEnable(true);
        pdsVolcurFactor.setMonitorInterval(new Long(6));
        pdsVolcurFactor.setReportInterval(new Long(6));

        pdsVolcurFactor.setVolLowerLimit(new Long(6));
        pdsVolcurFactor.setVolUpperLimit(new Long(6));
        pdsVolcurFactor.setCurUpperLimit(new Long(6));
        pdsVolcurFactor.setCurLowerLimit(new Long(6));
        pdsVolcurFactor.setAlarmInterval(new Long(6));
        pdsVolcurFactor.setAlarmDelay(new Long(6));
        pdsVolcurFactor.setAlarmMsg(true);
        pdsVolcurFactor.setAlarmEmail(true);
        pdsVolcurFactorService.insertPdsVolcurFactor(pdsVolcurFactor);

        PdsChannelNode pdsChannelNode = new PdsChannelNode();
        pdsChannelNode.setChannelId(volcurFactorVo.getChannelId());
        pdsChannelNode.setNodeType("5");
        pdsChannelNode.setNodeId(pdsVolcurFactor.getId());
        int ret = pdsChannelNodeService.insertPdsChannelNode(pdsChannelNode);

        return toAjax(ret);
    }

    /**
     * 修改电压电流信息
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:edit')")
    @Log(title = "电压电流信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsVolcurFactor pdsVolcurFactor)
    {
        return toAjax(pdsVolcurFactorService.updatePdsVolcurFactor(pdsVolcurFactor));
    }

    /**
     * 删除电压电流信息
     */
    @PreAuthorize("@ss.hasPermi('devices:volcur_factor:remove')")
    @Log(title = "电压电流信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestBody PdsChannelNode pdsChannelNode)
    {
        pdsChannelNodeService.deleteByChannelIdAndNodeId(pdsChannelNode);
        int ret = pdsVolcurFactorService.deletePdsVolcurFactorById(pdsChannelNode.getNodeId());
        return toAjax(ret);
    }
}
