package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SeverHost;
import com.ruoyi.system.service.ISeverHostService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * hostController
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/data_config/host")
public class SeverHostController extends BaseController
{
    @Autowired
    private ISeverHostService severHostService;

    /**
     * 查询host列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:list')")
    @GetMapping("/list")
    public TableDataInfo list(SeverHost severHost)
    {
        startPage();
        List<SeverHost> list = severHostService.selectSeverHostList(severHost);
        return getDataTable(list);
    }

    /**
     * 导出host列表
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:export')")
    @Log(title = "host", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SeverHost severHost)
    {
        List<SeverHost> list = severHostService.selectSeverHostList(severHost);
        ExcelUtil<SeverHost> util = new ExcelUtil<SeverHost>(SeverHost.class);
        util.exportExcel(response, list, "host数据");
    }

    /**
     * 获取host详细信息
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(severHostService.selectSeverHostById(id));
    }

    /**
     * 新增host
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:add')")
    @Log(title = "host", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SeverHost severHost)
    {
        return toAjax(severHostService.insertSeverHost(severHost));
    }

    /**
     * 修改host
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:edit')")
    @Log(title = "host", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SeverHost severHost)
    {
        return toAjax(severHostService.updateSeverHost(severHost));
    }

    /**
     * 删除host
     */
    @PreAuthorize("@ss.hasPermi('data_config:host:remove')")
    @Log(title = "host", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(severHostService.deleteSeverHostByIds(ids));
    }
}
