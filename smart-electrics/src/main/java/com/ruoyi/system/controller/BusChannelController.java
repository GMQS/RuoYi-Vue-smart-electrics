package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusChannel;
import com.ruoyi.system.service.IBusChannelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 管道Controller
 * 
 * @author yk
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/system/channel_info")
public class BusChannelController extends BaseController
{
    @Autowired
    private IBusChannelService busChannelService;

    /**
     * 查询管道列表
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusChannel busChannel)
    {
        startPage();
        List<BusChannel> list = busChannelService.selectBusChannelList(busChannel);
        return getDataTable(list);
    }

    /**
     * 导出管道列表
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:export')")
    @Log(title = "管道", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusChannel busChannel)
    {
        List<BusChannel> list = busChannelService.selectBusChannelList(busChannel);
        ExcelUtil<BusChannel> util = new ExcelUtil<BusChannel>(BusChannel.class);
        util.exportExcel(response, list, "管道数据");
    }

    /**
     * 获取管道详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(busChannelService.selectBusChannelById(id));
    }

    /**
     * 新增管道
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:add')")
    @Log(title = "管道", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusChannel busChannel)
    {
        return toAjax(busChannelService.insertBusChannel(busChannel));
    }

    /**
     * 修改管道
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:edit')")
    @Log(title = "管道", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusChannel busChannel)
    {
        return toAjax(busChannelService.updateBusChannel(busChannel));
    }

    /**
     * 删除管道
     */
    @PreAuthorize("@ss.hasPermi('system:channel_info:remove')")
    @Log(title = "管道", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busChannelService.deleteBusChannelByIds(ids));
    }
}
