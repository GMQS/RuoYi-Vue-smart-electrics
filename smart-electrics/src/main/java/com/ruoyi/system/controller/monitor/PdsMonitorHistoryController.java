package com.ruoyi.system.controller.monitor;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsMonitorHistory;
import com.ruoyi.system.service.IPdsMonitorHistoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 历史监控数据Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/monitor/monitor_history")
public class PdsMonitorHistoryController extends BaseController
{
    @Autowired
    private IPdsMonitorHistoryService pdsMonitorHistoryService;

    /**
     * 查询历史监控数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsMonitorHistory pdsMonitorHistory)
    {
        startPage();
        List<PdsMonitorHistory> list = pdsMonitorHistoryService.selectPdsMonitorHistoryList(pdsMonitorHistory);
        return getDataTable(list);
    }

    /**
     * 导出历史监控数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:export')")
    @Log(title = "历史监控数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsMonitorHistory pdsMonitorHistory)
    {
        List<PdsMonitorHistory> list = pdsMonitorHistoryService.selectPdsMonitorHistoryList(pdsMonitorHistory);
        ExcelUtil<PdsMonitorHistory> util = new ExcelUtil<PdsMonitorHistory>(PdsMonitorHistory.class);
        util.exportExcel(response, list, "历史监控数据数据");
    }

    /**
     * 获取历史监控数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsMonitorHistoryService.selectPdsMonitorHistoryById(id));
    }

    /**
     * 新增历史监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:add')")
    @Log(title = "历史监控数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsMonitorHistory pdsMonitorHistory)
    {
        return toAjax(pdsMonitorHistoryService.insertPdsMonitorHistory(pdsMonitorHistory));
    }

    /**
     * 修改历史监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:edit')")
    @Log(title = "历史监控数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsMonitorHistory pdsMonitorHistory)
    {
        return toAjax(pdsMonitorHistoryService.updatePdsMonitorHistory(pdsMonitorHistory));
    }

    /**
     * 删除历史监控数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitor_history:remove')")
    @Log(title = "历史监控数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsMonitorHistoryService.deletePdsMonitorHistoryByIds(ids));
    }
}
