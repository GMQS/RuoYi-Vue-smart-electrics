package com.ruoyi.system.controller.alarm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.domain.PdsAllAlarmInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PdsAlarmInfo;
import com.ruoyi.system.service.IPdsAlarmInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 告警信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/alarm/alarm_info")
public class PdsAlarmInfoController extends BaseController
{
    @Autowired
    private IPdsAlarmInfoService pdsAlarmInfoService;

    /**
     * 查询告警信息列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:list')")
    @GetMapping("/list")
    public TableDataInfo list(PdsAlarmInfo pdsAlarmInfo)
    {
        startPage();
        List<PdsAlarmInfo> list = pdsAlarmInfoService.selectPdsAlarmInfoList(pdsAlarmInfo);
        return getDataTable(list);
    }

    /**
     * 条件统计
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:list')")
    @GetMapping("/statistics")
    public AjaxResult statistics(PdsAlarmInfo pdsAlarmInfo)
    {
        List<PdsAlarmInfo> list = pdsAlarmInfoService.selectPdsAlarmInfoList(pdsAlarmInfo);
        JSONObject ret = new JSONObject();
        ret.put("total", list.size());
        JSONArray projectArray = new JSONArray();
        Map<String, Long> projectCount = list.stream().
                collect(Collectors.groupingBy(PdsAlarmInfo::getProjectName, Collectors.counting()));
        projectCount.forEach((key, value) -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", key);
            jsonObject.put("value", value);
            projectArray.add(jsonObject);
        });
        ret.put("project", projectArray);
        JSONArray rankArray = new JSONArray();
        Map<String, Long> rankCount = list.stream().
                collect(Collectors.groupingBy(PdsAlarmInfo::getAlarmRank, Collectors.counting()));
        rankArray.clear();
        rankCount.forEach((key, value) -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", key);
            jsonObject.put("value", value);
            rankArray.add(jsonObject);
        });
        ret.put("rank", rankArray);
        JSONArray stateArray = new JSONArray();
        Map<String, Long> stateCount = list.stream().
                collect(Collectors.groupingBy(PdsAlarmInfo::getAlarmState, Collectors.counting()));
        stateArray.clear();
        stateCount.forEach((key, value) -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", key);
            jsonObject.put("value", value);
            stateArray.add(jsonObject);
        });
        ret.put("state", stateArray);
        return success(ret);
//        Map<String, Long> test = new HashMap<>();
//        test.put("ret", new Long(123));
//        return success(test);
    }

    /**
     * 导出告警信息列表
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:export')")
    @Log(title = "告警信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdsAlarmInfo pdsAlarmInfo)
    {
        List<PdsAlarmInfo> list = pdsAlarmInfoService.selectPdsAlarmInfoList(pdsAlarmInfo);
        ExcelUtil<PdsAlarmInfo> util = new ExcelUtil<PdsAlarmInfo>(PdsAlarmInfo.class);
        util.exportExcel(response, list, "告警信息数据");
    }

    /**
     * 获取告警信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdsAlarmInfoService.selectPdsAlarmInfoById(id));
    }

    /**
     * 新增告警信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:add')")
    @Log(title = "告警信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdsAlarmInfo pdsAlarmInfo)
    {
        return toAjax(pdsAlarmInfoService.insertPdsAlarmInfo(pdsAlarmInfo));
    }

    /**
     * 修改告警信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:edit')")
    @Log(title = "告警信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdsAlarmInfo pdsAlarmInfo)
    {
        return toAjax(pdsAlarmInfoService.updatePdsAlarmInfo(pdsAlarmInfo));
    }

    /**
     * 删除告警信息
     */
    @PreAuthorize("@ss.hasPermi('alarm:alarm_info:remove')")
    @Log(title = "告警信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdsAlarmInfoService.deletePdsAlarmInfoByIds(ids));
    }
}
