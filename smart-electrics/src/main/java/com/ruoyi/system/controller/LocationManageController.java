package com.ruoyi.system.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.idList.LocationIdInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LocationManage;
import com.ruoyi.system.service.ILocationManageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * location_manageController
 * 
 * @author yk
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/devices/location_manage")
public class LocationManageController extends BaseController
{
    @Autowired
    private ILocationManageService locationManageService;

    /**
     * 查询location_manage列表
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:list')")
    @GetMapping("/list")
    public TableDataInfo list(LocationManage locationManage)
    {
        startPage();
        List<LocationManage> list = locationManageService.selectLocationManageList(locationManage);
        return getDataTable(list);
    }

    /**
     * 查询项归属区id列表,可以给对应的配电箱设置归属区id时使用
     * @param locationManage
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:list')")
    @GetMapping("/locationIds")
    public List<LocationIdInfo> locationIdList(LocationManage locationManage){
        List<LocationManage> list = locationManageService.selectLocationManageList(locationManage);
        List<LocationIdInfo> anslist = new ArrayList<>();
        for(LocationManage location :list){
            String name=location.getLocationName();
            Long id=location.getId();
            LocationIdInfo locationIdInfo=new LocationIdInfo();
            locationIdInfo.setLocationId(id);
            locationIdInfo.setLocationName(name);
            anslist.add(locationIdInfo);
        }
        return anslist;
    }

    /**
     * 导出location_manage列表
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:export')")
    @Log(title = "location_manage", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LocationManage locationManage)
    {
        List<LocationManage> list = locationManageService.selectLocationManageList(locationManage);
        ExcelUtil<LocationManage> util = new ExcelUtil<LocationManage>(LocationManage.class);
        util.exportExcel(response, list, "location_manage数据");
    }

    /**
     * 获取location_manage详细信息
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(locationManageService.selectLocationManageById(id));
    }

    /**
     * 新增location_manage
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:add')")
    @Log(title = "location_manage", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LocationManage locationManage)
    {
        return toAjax(locationManageService.insertLocationManage(locationManage));
    }

    /**
     * 修改location_manage
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:edit')")
    @Log(title = "location_manage", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LocationManage locationManage)
    {
        return toAjax(locationManageService.updateLocationManage(locationManage));
    }

    /**
     * 删除location_manage
     */
    @PreAuthorize("@ss.hasPermi('devices:location_manage:remove')")
    @Log(title = "location_manage", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(locationManageService.deleteLocationManageByIds(ids));
    }
}
