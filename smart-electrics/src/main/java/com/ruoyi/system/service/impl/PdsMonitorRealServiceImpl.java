package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsMonitorRealMapper;
import com.ruoyi.system.domain.PdsMonitorReal;
import com.ruoyi.system.service.IPdsMonitorRealService;

/**
 * 实时监控数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class PdsMonitorRealServiceImpl implements IPdsMonitorRealService 
{
    @Autowired
    private PdsMonitorRealMapper pdsMonitorRealMapper;

    /**
     * 查询实时监控数据
     * 
     * @param id 实时监控数据主键
     * @return 实时监控数据
     */
    @Override
    public PdsMonitorReal selectPdsMonitorRealById(Long id)
    {
        return pdsMonitorRealMapper.selectPdsMonitorRealById(id);
    }

    /**
     * 查询实时监控数据列表
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 实时监控数据
     */
    @Override
    public List<PdsMonitorReal> selectPdsMonitorRealList(PdsMonitorReal pdsMonitorReal)
    {
        return pdsMonitorRealMapper.selectPdsMonitorRealList(pdsMonitorReal);
    }

    /**
     * 新增实时监控数据
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 结果
     */
    @Override
    public int insertPdsMonitorReal(PdsMonitorReal pdsMonitorReal)
    {
        return pdsMonitorRealMapper.insertPdsMonitorReal(pdsMonitorReal);
    }

    /**
     * 修改实时监控数据
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 结果
     */
    @Override
    public int updatePdsMonitorReal(PdsMonitorReal pdsMonitorReal)
    {
        return pdsMonitorRealMapper.updatePdsMonitorReal(pdsMonitorReal);
    }

    /**
     * 批量删除实时监控数据
     * 
     * @param ids 需要删除的实时监控数据主键
     * @return 结果
     */
    @Override
    public int deletePdsMonitorRealByIds(Long[] ids)
    {
        return pdsMonitorRealMapper.deletePdsMonitorRealByIds(ids);
    }

    /**
     * 删除实时监控数据信息
     * 
     * @param id 实时监控数据主键
     * @return 结果
     */
    @Override
    public int deletePdsMonitorRealById(Long id)
    {
        return pdsMonitorRealMapper.deletePdsMonitorRealById(id);
    }
}
