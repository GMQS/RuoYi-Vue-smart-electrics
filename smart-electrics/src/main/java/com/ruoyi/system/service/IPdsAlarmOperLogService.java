package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsAlarmOperLog;

/**
 * 告警操作日志Service接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public interface IPdsAlarmOperLogService 
{
    /**
     * 查询告警操作日志
     * 
     * @param id 告警操作日志主键
     * @return 告警操作日志
     */
    public PdsAlarmOperLog selectPdsAlarmOperLogById(Long id);

    /**
     * 查询告警操作日志列表
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 告警操作日志集合
     */
    public List<PdsAlarmOperLog> selectPdsAlarmOperLogList(PdsAlarmOperLog pdsAlarmOperLog);

    /**
     * 新增告警操作日志
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 结果
     */
    public int insertPdsAlarmOperLog(PdsAlarmOperLog pdsAlarmOperLog);

    /**
     * 修改告警操作日志
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 结果
     */
    public int updatePdsAlarmOperLog(PdsAlarmOperLog pdsAlarmOperLog);

    /**
     * 批量删除告警操作日志
     * 
     * @param ids 需要删除的告警操作日志主键集合
     * @return 结果
     */
    public int deletePdsAlarmOperLogByIds(Long[] ids);

    /**
     * 删除告警操作日志信息
     * 
     * @param id 告警操作日志主键
     * @return 结果
     */
    public int deletePdsAlarmOperLogById(Long id);
}
