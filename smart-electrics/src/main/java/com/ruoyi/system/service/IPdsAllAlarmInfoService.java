package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsAllAlarmInfo;

/**
 * 详细告警记录Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface IPdsAllAlarmInfoService 
{
    /**
     * 查询详细告警记录
     * 
     * @param id 详细告警记录主键
     * @return 详细告警记录
     */
    public PdsAllAlarmInfo selectPdsAllAlarmInfoById(Long id);

    /**
     * 查询详细告警记录列表
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 详细告警记录集合
     */
    public List<PdsAllAlarmInfo> selectPdsAllAlarmInfoList(PdsAllAlarmInfo pdsAllAlarmInfo);

    /**
     * 新增详细告警记录
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 结果
     */
    public int insertPdsAllAlarmInfo(PdsAllAlarmInfo pdsAllAlarmInfo);

    /**
     * 修改详细告警记录
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 结果
     */
    public int updatePdsAllAlarmInfo(PdsAllAlarmInfo pdsAllAlarmInfo);

    /**
     * 批量删除详细告警记录
     * 
     * @param ids 需要删除的详细告警记录主键集合
     * @return 结果
     */
    public int deletePdsAllAlarmInfoByIds(Long[] ids);

    /**
     * 删除详细告警记录信息
     * 
     * @param id 详细告警记录主键
     * @return 结果
     */
    public int deletePdsAllAlarmInfoById(Long id);
}
