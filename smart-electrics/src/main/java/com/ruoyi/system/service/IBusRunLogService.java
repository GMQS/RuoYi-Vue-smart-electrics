package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusRunLog;

/**
 * run_logService接口
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
public interface IBusRunLogService 
{
    /**
     * 查询run_log
     * 
     * @param id run_log主键
     * @return run_log
     */
    public BusRunLog selectBusRunLogById(Long id);

    /**
     * 查询run_log列表
     * 
     * @param busRunLog run_log
     * @return run_log集合
     */
    public List<BusRunLog> selectBusRunLogList(BusRunLog busRunLog);

    /**
     * 新增run_log
     * 
     * @param busRunLog run_log
     * @return 结果
     */
    public int insertBusRunLog(BusRunLog busRunLog);

    /**
     * 修改run_log
     * 
     * @param busRunLog run_log
     * @return 结果
     */
    public int updateBusRunLog(BusRunLog busRunLog);

    /**
     * 批量删除run_log
     * 
     * @param ids 需要删除的run_log主键集合
     * @return 结果
     */
    public int deleteBusRunLogByIds(Long[] ids);

    /**
     * 删除run_log信息
     * 
     * @param id run_log主键
     * @return 结果
     */
    public int deleteBusRunLogById(Long id);
}
