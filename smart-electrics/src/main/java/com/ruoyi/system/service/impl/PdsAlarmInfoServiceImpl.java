package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsAlarmInfoMapper;
import com.ruoyi.system.domain.PdsAlarmInfo;
import com.ruoyi.system.service.IPdsAlarmInfoService;

/**
 * 告警信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class PdsAlarmInfoServiceImpl implements IPdsAlarmInfoService 
{
    @Autowired
    private PdsAlarmInfoMapper pdsAlarmInfoMapper;

    /**
     * 查询告警信息
     * 
     * @param id 告警信息主键
     * @return 告警信息
     */
    @Override
    public PdsAlarmInfo selectPdsAlarmInfoById(Long id)
    {
        return pdsAlarmInfoMapper.selectPdsAlarmInfoById(id);
    }

    /**
     * 查询告警信息列表
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 告警信息
     */
    @Override
    public List<PdsAlarmInfo> selectPdsAlarmInfoList(PdsAlarmInfo pdsAlarmInfo)
    {
        return pdsAlarmInfoMapper.selectPdsAlarmInfoList(pdsAlarmInfo);
    }

    /**
     * 新增告警信息
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 结果
     */
    @Override
    public int insertPdsAlarmInfo(PdsAlarmInfo pdsAlarmInfo)
    {
        return pdsAlarmInfoMapper.insertPdsAlarmInfo(pdsAlarmInfo);
    }

    /**
     * 修改告警信息
     * 
     * @param pdsAlarmInfo 告警信息
     * @return 结果
     */
    @Override
    public int updatePdsAlarmInfo(PdsAlarmInfo pdsAlarmInfo)
    {
        return pdsAlarmInfoMapper.updatePdsAlarmInfo(pdsAlarmInfo);
    }

    /**
     * 批量删除告警信息
     * 
     * @param ids 需要删除的告警信息主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmInfoByIds(Long[] ids)
    {
        return pdsAlarmInfoMapper.deletePdsAlarmInfoByIds(ids);
    }

    /**
     * 删除告警信息信息
     * 
     * @param id 告警信息主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmInfoById(Long id)
    {
        return pdsAlarmInfoMapper.deletePdsAlarmInfoById(id);
    }
}
