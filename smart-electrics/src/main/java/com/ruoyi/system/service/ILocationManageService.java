package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LocationManage;

/**
 * location_manageService接口
 * 
 * @author yk
 * @date 2024-03-07
 */
public interface ILocationManageService 
{
    /**
     * 查询location_manage
     * 
     * @param id location_manage主键
     * @return location_manage
     */
    public LocationManage selectLocationManageById(Long id);

    /**
     * 查询location_manage列表
     * 
     * @param locationManage location_manage
     * @return location_manage集合
     */
    public List<LocationManage> selectLocationManageList(LocationManage locationManage);

    /**
     * 新增location_manage
     * 
     * @param locationManage location_manage
     * @return 结果
     */
    public int insertLocationManage(LocationManage locationManage);

    /**
     * 修改location_manage
     * 
     * @param locationManage location_manage
     * @return 结果
     */
    public int updateLocationManage(LocationManage locationManage);

    /**
     * 批量删除location_manage
     * 
     * @param ids 需要删除的location_manage主键集合
     * @return 结果
     */
    public int deleteLocationManageByIds(Long[] ids);

    /**
     * 删除location_manage信息
     * 
     * @param id location_manage主键
     * @return 结果
     */
    public int deleteLocationManageById(Long id);
}
