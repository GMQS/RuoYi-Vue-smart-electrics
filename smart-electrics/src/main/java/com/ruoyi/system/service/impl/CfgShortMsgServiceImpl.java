package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CfgShortMsgMapper;
import com.ruoyi.system.domain.CfgShortMsg;
import com.ruoyi.system.service.ICfgShortMsgService;

/**
 * msgService业务层处理
 * 
 * @author xyq
 * @date 2024-04-29
 */
@Service
public class CfgShortMsgServiceImpl implements ICfgShortMsgService 
{
    @Autowired
    private CfgShortMsgMapper cfgShortMsgMapper;

    /**
     * 查询msg
     * 
     * @param id msg主键
     * @return msg
     */
    @Override
    public CfgShortMsg selectCfgShortMsgById(Long id)
    {
        return cfgShortMsgMapper.selectCfgShortMsgById(id);
    }

    /**
     * 查询msg列表
     * 
     * @param cfgShortMsg msg
     * @return msg
     */
    @Override
    public List<CfgShortMsg> selectCfgShortMsgList(CfgShortMsg cfgShortMsg)
    {
        return cfgShortMsgMapper.selectCfgShortMsgList(cfgShortMsg);
    }

    /**
     * 新增msg
     * 
     * @param cfgShortMsg msg
     * @return 结果
     */
    @Override
    public int insertCfgShortMsg(CfgShortMsg cfgShortMsg)
    {
        return cfgShortMsgMapper.insertCfgShortMsg(cfgShortMsg);
    }

    /**
     * 修改msg
     * 
     * @param cfgShortMsg msg
     * @return 结果
     */
    @Override
    public int updateCfgShortMsg(CfgShortMsg cfgShortMsg)
    {
        return cfgShortMsgMapper.updateCfgShortMsg(cfgShortMsg);
    }

    /**
     * 批量删除msg
     * 
     * @param ids 需要删除的msg主键
     * @return 结果
     */
    @Override
    public int deleteCfgShortMsgByIds(Long[] ids)
    {
        return cfgShortMsgMapper.deleteCfgShortMsgByIds(ids);
    }

    /**
     * 删除msg信息
     * 
     * @param id msg主键
     * @return 结果
     */
    @Override
    public int deleteCfgShortMsgById(Long id)
    {
        return cfgShortMsgMapper.deleteCfgShortMsgById(id);
    }
}
