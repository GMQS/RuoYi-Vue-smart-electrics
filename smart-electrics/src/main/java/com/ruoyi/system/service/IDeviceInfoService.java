package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DeviceInfo;

/**
 * 配电箱管理Service接口
 * 
 * @author yk
 * @date 2024-04-24
 */
public interface IDeviceInfoService 
{
    /**
     * 查询配电箱管理
     * 
     * @param id 配电箱管理主键
     * @return 配电箱管理
     */
    public DeviceInfo selectDeviceInfoById(String id);

    /**
     * 查询配电箱管理列表
     * 
     * @param deviceInfo 配电箱管理
     * @return 配电箱管理集合
     */
    public List<DeviceInfo> selectDeviceInfoList(DeviceInfo deviceInfo);

    /**
     * 新增配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    public int insertDeviceInfo(DeviceInfo deviceInfo);

    /**
     * 修改配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    public int updateDeviceInfo(DeviceInfo deviceInfo);

    /**
     * 批量删除配电箱管理
     * 
     * @param ids 需要删除的配电箱管理主键集合
     * @return 结果
     */
    public int deleteDeviceInfoByIds(String[] ids);

    /**
     * 删除配电箱管理信息
     * 
     * @param id 配电箱管理主键
     * @return 结果
     */
    public int deleteDeviceInfoById(String id);
}
