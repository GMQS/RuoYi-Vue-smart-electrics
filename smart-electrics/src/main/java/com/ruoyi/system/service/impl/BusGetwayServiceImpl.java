package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusGetwayMapper;
import com.ruoyi.system.domain.BusGetway;
import com.ruoyi.system.service.IBusGetwayService;

/**
 * 网关
Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class BusGetwayServiceImpl implements IBusGetwayService 
{
    @Autowired
    private BusGetwayMapper busGetwayMapper;

    /**
     * 查询网关

     * 
     * @param id 网关
主键
     * @return 网关

     */
    @Override
    public BusGetway selectBusGetwayById(String id)
    {
        return busGetwayMapper.selectBusGetwayById(id);
    }

    /**
     * 查询网关
列表
     * 
     * @param busGetway 网关

     * @return 网关

     */
    @Override
    public List<BusGetway> selectBusGetwayList(BusGetway busGetway)
    {
        return busGetwayMapper.selectBusGetwayList(busGetway);
    }

    /**
     * 新增网关

     * 
     * @param busGetway 网关

     * @return 结果
     */
    @Override
    public int insertBusGetway(BusGetway busGetway)
    {
        busGetway.setCreateTime(DateUtils.getNowDate());
        busGetway.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return busGetwayMapper.insertBusGetway(busGetway);
    }

    /**
     * 修改网关

     * 
     * @param busGetway 网关

     * @return 结果
     */
    @Override
    public int updateBusGetway(BusGetway busGetway)
    {
        busGetway.setUpdateTime(DateUtils.getNowDate());
        busGetway.setUpdateBy(SecurityUtils.getUsername());

        System.out.println(SecurityUtils.getUsername());
        return busGetwayMapper.updateBusGetway(busGetway);
    }

    /**
     * 批量删除网关

     * 
     * @param ids 需要删除的网关
主键
     * @return 结果
     */
    @Override
    public int deleteBusGetwayByIds(String[] ids)
    {
        return busGetwayMapper.deleteBusGetwayByIds(ids);
    }

    /**
     * 删除网关
信息
     * 
     * @param id 网关
主键
     * @return 结果
     */
    @Override
    public int deleteBusGetwayById(String id)
    {
        return busGetwayMapper.deleteBusGetwayById(id);
    }
}
