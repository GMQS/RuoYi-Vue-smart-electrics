package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SeverHostMapper;
import com.ruoyi.system.domain.SeverHost;
import com.ruoyi.system.service.ISeverHostService;

/**
 * hostService业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
@Service
public class SeverHostServiceImpl implements ISeverHostService 
{
    @Autowired
    private SeverHostMapper severHostMapper;

    /**
     * 查询host
     * 
     * @param id host主键
     * @return host
     */
    @Override
    public SeverHost selectSeverHostById(Long id)
    {
        return severHostMapper.selectSeverHostById(id);
    }

    /**
     * 查询host列表
     * 
     * @param severHost host
     * @return host
     */
    @Override
    public List<SeverHost> selectSeverHostList(SeverHost severHost)
    {
        return severHostMapper.selectSeverHostList(severHost);
    }

    /**
     * 新增host
     * 
     * @param severHost host
     * @return 结果
     */
    @Override
    public int insertSeverHost(SeverHost severHost)
    {
        return severHostMapper.insertSeverHost(severHost);
    }

    /**
     * 修改host
     * 
     * @param severHost host
     * @return 结果
     */
    @Override
    public int updateSeverHost(SeverHost severHost)
    {
        return severHostMapper.updateSeverHost(severHost);
    }

    /**
     * 批量删除host
     * 
     * @param ids 需要删除的host主键
     * @return 结果
     */
    @Override
    public int deleteSeverHostByIds(Long[] ids)
    {
        return severHostMapper.deleteSeverHostByIds(ids);
    }

    /**
     * 删除host信息
     * 
     * @param id host主键
     * @return 结果
     */
    @Override
    public int deleteSeverHostById(Long id)
    {
        return severHostMapper.deleteSeverHostById(id);
    }
}
