package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.CfgEmail;

/**
 * 告警配置Service接口
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public interface ICfgEmailService 
{
    /**
     * 查询告警配置
     * 
     * @param id 告警配置主键
     * @return 告警配置
     */
    public CfgEmail selectCfgEmailById(Long id);

    /**
     * 查询告警配置列表
     * 
     * @param cfgEmail 告警配置
     * @return 告警配置集合
     */
    public List<CfgEmail> selectCfgEmailList(CfgEmail cfgEmail);

    /**
     * 新增告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    public int insertCfgEmail(CfgEmail cfgEmail);

    /**
     * 修改告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    public int updateCfgEmail(CfgEmail cfgEmail);

    /**
     * 批量删除告警配置
     * 
     * @param ids 需要删除的告警配置主键集合
     * @return 结果
     */
    public int deleteCfgEmailByIds(Long[] ids);

    /**
     * 删除告警配置信息
     * 
     * @param id 告警配置主键
     * @return 结果
     */
    public int deleteCfgEmailById(Long id);
}
