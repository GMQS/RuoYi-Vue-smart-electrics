package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ScheduleJobLogMapper;
import com.ruoyi.system.domain.ScheduleJobLog;
import com.ruoyi.system.service.IScheduleJobLogService;

/**
 * 定时任务日志Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
@Service
public class ScheduleJobLogServiceImpl implements IScheduleJobLogService 
{
    @Autowired
    private ScheduleJobLogMapper scheduleJobLogMapper;

    /**
     * 查询定时任务日志
     * 
     * @param id 定时任务日志主键
     * @return 定时任务日志
     */
    @Override
    public ScheduleJobLog selectScheduleJobLogById(Long id)
    {
        return scheduleJobLogMapper.selectScheduleJobLogById(id);
    }

    /**
     * 查询定时任务日志列表
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 定时任务日志
     */
    @Override
    public List<ScheduleJobLog> selectScheduleJobLogList(ScheduleJobLog scheduleJobLog)
    {
        return scheduleJobLogMapper.selectScheduleJobLogList(scheduleJobLog);
    }

    /**
     * 新增定时任务日志
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 结果
     */
    @Override
    public int insertScheduleJobLog(ScheduleJobLog scheduleJobLog)
    {
        scheduleJobLog.setCreateTime(DateUtils.getNowDate());
        return scheduleJobLogMapper.insertScheduleJobLog(scheduleJobLog);
    }

    /**
     * 修改定时任务日志
     * 
     * @param scheduleJobLog 定时任务日志
     * @return 结果
     */
    @Override
    public int updateScheduleJobLog(ScheduleJobLog scheduleJobLog)
    {
        scheduleJobLog.setUpdateTime(DateUtils.getNowDate());
        return scheduleJobLogMapper.updateScheduleJobLog(scheduleJobLog);
    }

    /**
     * 批量删除定时任务日志
     * 
     * @param ids 需要删除的定时任务日志主键
     * @return 结果
     */
    @Override
    public int deleteScheduleJobLogByIds(Long[] ids)
    {
        return scheduleJobLogMapper.deleteScheduleJobLogByIds(ids);
    }

    /**
     * 删除定时任务日志信息
     * 
     * @param id 定时任务日志主键
     * @return 结果
     */
    @Override
    public int deleteScheduleJobLogById(Long id)
    {
        return scheduleJobLogMapper.deleteScheduleJobLogById(id);
    }
}
