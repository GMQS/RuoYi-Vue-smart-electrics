package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsChannelNodeMapper;
import com.ruoyi.system.domain.PdsChannelNode;
import com.ruoyi.system.service.IPdsChannelNodeService;

/**
 * 管道节点关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsChannelNodeServiceImpl implements IPdsChannelNodeService 
{
    @Autowired
    private PdsChannelNodeMapper pdsChannelNodeMapper;

    /**
     * 查询管道节点关联
     * 
     * @param id 管道节点关联主键
     * @return 管道节点关联
     */
    @Override
    public PdsChannelNode selectPdsChannelNodeById(Long id)
    {
        return pdsChannelNodeMapper.selectPdsChannelNodeById(id);
    }

    /**
     * 查询管道节点关联列表
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 管道节点关联
     */
    @Override
    public List<PdsChannelNode> selectPdsChannelNodeList(PdsChannelNode pdsChannelNode)
    {
        return pdsChannelNodeMapper.selectPdsChannelNodeList(pdsChannelNode);
    }

    /**
     * 查询管道下所有节点
     *
     * @param channelId 管道id
     * @return 管道节点关联集合
     */
    @Override
    public List<PdsChannelNode> selectPdsChannelNodeListByChannelId(Long channelId){
        return pdsChannelNodeMapper.selectPdsChannelNodeListByChannelId(channelId);
    };

    /**
     * 新增管道节点关联
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 结果
     */
    @Override
    public int insertPdsChannelNode(PdsChannelNode pdsChannelNode)
    {
        return pdsChannelNodeMapper.insertPdsChannelNode(pdsChannelNode);
    }

    /**
     * 修改管道节点关联
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 结果
     */
    @Override
    public int updatePdsChannelNode(PdsChannelNode pdsChannelNode)
    {
        return pdsChannelNodeMapper.updatePdsChannelNode(pdsChannelNode);
    }

    /**
     * 批量删除管道节点关联
     * 
     * @param ids 需要删除的管道节点关联主键
     * @return 结果
     */
    @Override
    public int deletePdsChannelNodeByIds(Long[] ids)
    {
        return pdsChannelNodeMapper.deletePdsChannelNodeByIds(ids);
    }

    /**
     * 删除管道节点关联信息
     * 
     * @param id 管道节点关联主键
     * @return 结果
     */
    @Override
    public int deletePdsChannelNodeById(Long id)
    {
        return pdsChannelNodeMapper.deletePdsChannelNodeById(id);
    }


    /**
     * 根据管道id和节点id删除管道节点关联
     *
     * @return 结果
     */
    @Override
    public int deleteByChannelIdAndNodeId(PdsChannelNode pdsChannelNode){
        return pdsChannelNodeMapper.deleteByChannelIdAndNodeId(pdsChannelNode);
    };
}
