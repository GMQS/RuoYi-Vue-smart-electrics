package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsAllAlarmInfoMapper;
import com.ruoyi.system.domain.PdsAllAlarmInfo;
import com.ruoyi.system.service.IPdsAllAlarmInfoService;

/**
 * 详细告警记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class PdsAllAlarmInfoServiceImpl implements IPdsAllAlarmInfoService 
{
    @Autowired
    private PdsAllAlarmInfoMapper pdsAllAlarmInfoMapper;

    /**
     * 查询详细告警记录
     * 
     * @param id 详细告警记录主键
     * @return 详细告警记录
     */
    @Override
    public PdsAllAlarmInfo selectPdsAllAlarmInfoById(Long id)
    {
        return pdsAllAlarmInfoMapper.selectPdsAllAlarmInfoById(id);
    }

    /**
     * 查询详细告警记录列表
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 详细告警记录
     */
    @Override
    public List<PdsAllAlarmInfo> selectPdsAllAlarmInfoList(PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        return pdsAllAlarmInfoMapper.selectPdsAllAlarmInfoList(pdsAllAlarmInfo);
    }

    /**
     * 新增详细告警记录
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 结果
     */
    @Override
    public int insertPdsAllAlarmInfo(PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        return pdsAllAlarmInfoMapper.insertPdsAllAlarmInfo(pdsAllAlarmInfo);
    }

    /**
     * 修改详细告警记录
     * 
     * @param pdsAllAlarmInfo 详细告警记录
     * @return 结果
     */
    @Override
    public int updatePdsAllAlarmInfo(PdsAllAlarmInfo pdsAllAlarmInfo)
    {
        return pdsAllAlarmInfoMapper.updatePdsAllAlarmInfo(pdsAllAlarmInfo);
    }

    /**
     * 批量删除详细告警记录
     * 
     * @param ids 需要删除的详细告警记录主键
     * @return 结果
     */
    @Override
    public int deletePdsAllAlarmInfoByIds(Long[] ids)
    {
        return pdsAllAlarmInfoMapper.deletePdsAllAlarmInfoByIds(ids);
    }

    /**
     * 删除详细告警记录信息
     * 
     * @param id 详细告警记录主键
     * @return 结果
     */
    @Override
    public int deletePdsAllAlarmInfoById(Long id)
    {
        return pdsAllAlarmInfoMapper.deletePdsAllAlarmInfoById(id);
    }
}
