package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsNetworkFactorMapper;
import com.ruoyi.system.domain.PdsNetworkFactor;
import com.ruoyi.system.service.IPdsNetworkFactorService;

/**
 * 网路信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsNetworkFactorServiceImpl implements IPdsNetworkFactorService 
{
    @Autowired
    private PdsNetworkFactorMapper pdsNetworkFactorMapper;

    /**
     * 查询网路信息
     * 
     * @param id 网路信息主键
     * @return 网路信息
     */
    @Override
    public PdsNetworkFactor selectPdsNetworkFactorById(Long id)
    {
        return pdsNetworkFactorMapper.selectPdsNetworkFactorById(id);
    }

    /**
     * 查询网路信息列表
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 网路信息
     */
    @Override
    public List<PdsNetworkFactor> selectPdsNetworkFactorList(PdsNetworkFactor pdsNetworkFactor)
    {
        return pdsNetworkFactorMapper.selectPdsNetworkFactorList(pdsNetworkFactor);
    }

    /**
     * 新增网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    @Override
    public int insertPdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor)
    {
        return pdsNetworkFactorMapper.insertPdsNetworkFactor(pdsNetworkFactor);
    }

    /**
     * 修改网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    @Override
    public int updatePdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor)
    {
        return pdsNetworkFactorMapper.updatePdsNetworkFactor(pdsNetworkFactor);
    }

    /**
     * 批量删除网路信息
     * 
     * @param ids 需要删除的网路信息主键
     * @return 结果
     */
    @Override
    public int deletePdsNetworkFactorByIds(Long[] ids)
    {
        return pdsNetworkFactorMapper.deletePdsNetworkFactorByIds(ids);
    }

    /**
     * 删除网路信息信息
     * 
     * @param id 网路信息主键
     * @return 结果
     */
    @Override
    public int deletePdsNetworkFactorById(Long id)
    {
        return pdsNetworkFactorMapper.deletePdsNetworkFactorById(id);
    }
}
