package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsCtrlFactorMapper;
import com.ruoyi.system.domain.PdsCtrlFactor;
import com.ruoyi.system.service.IPdsCtrlFactorService;

/**
 * 电控信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsCtrlFactorServiceImpl implements IPdsCtrlFactorService 
{
    @Autowired
    private PdsCtrlFactorMapper pdsCtrlFactorMapper;

    /**
     * 查询电控信息
     * 
     * @param id 电控信息主键
     * @return 电控信息
     */
    @Override
    public PdsCtrlFactor selectPdsCtrlFactorById(Long id)
    {
        return pdsCtrlFactorMapper.selectPdsCtrlFactorById(id);
    }

    /**
     * 查询电控信息列表
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 电控信息
     */
    @Override
    public List<PdsCtrlFactor> selectPdsCtrlFactorList(PdsCtrlFactor pdsCtrlFactor)
    {
        return pdsCtrlFactorMapper.selectPdsCtrlFactorList(pdsCtrlFactor);
    }

    /**
     * 新增电控信息
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 结果
     */
    @Override
    public int insertPdsCtrlFactor(PdsCtrlFactor pdsCtrlFactor)
    {
        return pdsCtrlFactorMapper.insertPdsCtrlFactor(pdsCtrlFactor);
    }

    /**
     * 修改电控信息
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 结果
     */
    @Override
    public int updatePdsCtrlFactor(PdsCtrlFactor pdsCtrlFactor)
    {
        return pdsCtrlFactorMapper.updatePdsCtrlFactor(pdsCtrlFactor);
    }

    /**
     * 批量删除电控信息
     * 
     * @param ids 需要删除的电控信息主键
     * @return 结果
     */
    @Override
    public int deletePdsCtrlFactorByIds(Long[] ids)
    {
        return pdsCtrlFactorMapper.deletePdsCtrlFactorByIds(ids);
    }

    /**
     * 删除电控信息信息
     * 
     * @param id 电控信息主键
     * @return 结果
     */
    @Override
    public int deletePdsCtrlFactorById(Long id)
    {
        return pdsCtrlFactorMapper.deletePdsCtrlFactorById(id);
    }
}
