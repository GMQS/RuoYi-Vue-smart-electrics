package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SeverHost;

/**
 * hostService接口
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
public interface ISeverHostService 
{
    /**
     * 查询host
     * 
     * @param id host主键
     * @return host
     */
    public SeverHost selectSeverHostById(Long id);

    /**
     * 查询host列表
     * 
     * @param severHost host
     * @return host集合
     */
    public List<SeverHost> selectSeverHostList(SeverHost severHost);

    /**
     * 新增host
     * 
     * @param severHost host
     * @return 结果
     */
    public int insertSeverHost(SeverHost severHost);

    /**
     * 修改host
     * 
     * @param severHost host
     * @return 结果
     */
    public int updateSeverHost(SeverHost severHost);

    /**
     * 批量删除host
     * 
     * @param ids 需要删除的host主键集合
     * @return 结果
     */
    public int deleteSeverHostByIds(Long[] ids);

    /**
     * 删除host信息
     * 
     * @param id host主键
     * @return 结果
     */
    public int deleteSeverHostById(Long id);
}
