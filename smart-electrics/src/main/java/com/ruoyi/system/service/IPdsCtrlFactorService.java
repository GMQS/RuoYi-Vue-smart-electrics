package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsCtrlFactor;

/**
 * 电控信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IPdsCtrlFactorService 
{
    /**
     * 查询电控信息
     * 
     * @param id 电控信息主键
     * @return 电控信息
     */
    public PdsCtrlFactor selectPdsCtrlFactorById(Long id);

    /**
     * 查询电控信息列表
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 电控信息集合
     */
    public List<PdsCtrlFactor> selectPdsCtrlFactorList(PdsCtrlFactor pdsCtrlFactor);

    /**
     * 新增电控信息
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 结果
     */
    public int insertPdsCtrlFactor(PdsCtrlFactor pdsCtrlFactor);

    /**
     * 修改电控信息
     * 
     * @param pdsCtrlFactor 电控信息
     * @return 结果
     */
    public int updatePdsCtrlFactor(PdsCtrlFactor pdsCtrlFactor);

    /**
     * 批量删除电控信息
     * 
     * @param ids 需要删除的电控信息主键集合
     * @return 结果
     */
    public int deletePdsCtrlFactorByIds(Long[] ids);

    /**
     * 删除电控信息信息
     * 
     * @param id 电控信息主键
     * @return 结果
     */
    public int deletePdsCtrlFactorById(Long id);
}
