package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceInfoMapper;
import com.ruoyi.system.domain.DeviceInfo;
import com.ruoyi.system.service.IDeviceInfoService;

/**
 * 配电箱管理Service业务层处理
 * 
 * @author yk
 * @date 2024-04-24
 */
@Service
public class DeviceInfoServiceImpl implements IDeviceInfoService 
{
    @Autowired
    private DeviceInfoMapper deviceInfoMapper;

    /**
     * 查询配电箱管理
     * 
     * @param id 配电箱管理主键
     * @return 配电箱管理
     */
    @Override
    public DeviceInfo selectDeviceInfoById(String id)
    {
        return deviceInfoMapper.selectDeviceInfoById(id);
    }

    /**
     * 查询配电箱管理列表
     * 
     * @param deviceInfo 配电箱管理
     * @return 配电箱管理
     */
    @Override
    public List<DeviceInfo> selectDeviceInfoList(DeviceInfo deviceInfo)
    {
        return deviceInfoMapper.selectDeviceInfoList(deviceInfo);
    }

    /**
     * 新增配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    @Override
    public int insertDeviceInfo(DeviceInfo deviceInfo)
    {
        deviceInfo.setCreateTime(DateUtils.getNowDate());
        deviceInfo.setCreateBy(SecurityUtils.getUsername());
        //如何获取后端的归属区id列表,和前端的设置的归属区id
        return deviceInfoMapper.insertDeviceInfo(deviceInfo);
    }

    /**
     * 修改配电箱管理
     * 
     * @param deviceInfo 配电箱管理
     * @return 结果
     */
    @Override
    public int updateDeviceInfo(DeviceInfo deviceInfo)
    {
        deviceInfo.setUpdateTime(DateUtils.getNowDate());
        deviceInfo.setUpdateBy(SecurityUtils.getUsername());
        return deviceInfoMapper.updateDeviceInfo(deviceInfo);
    }

    /**
     * 批量删除配电箱管理
     * 
     * @param ids 需要删除的配电箱管理主键
     * @return 结果
     */
    @Override
    public int deleteDeviceInfoByIds(String[] ids)
    {
        return deviceInfoMapper.deleteDeviceInfoByIds(ids);
    }

    /**
     * 删除配电箱管理信息
     * 
     * @param id 配电箱管理主键
     * @return 结果
     */
    @Override
    public int deleteDeviceInfoById(String id)
    {
        return deviceInfoMapper.deleteDeviceInfoById(id);
    }
}
