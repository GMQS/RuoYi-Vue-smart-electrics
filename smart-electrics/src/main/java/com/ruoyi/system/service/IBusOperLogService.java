package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusOperLog;

/**
 * oper_logService接口
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
public interface IBusOperLogService 
{
    /**
     * 查询oper_log
     * 
     * @param id oper_log主键
     * @return oper_log
     */
    public BusOperLog selectBusOperLogById(Long id);

    /**
     * 查询oper_log列表
     * 
     * @param busOperLog oper_log
     * @return oper_log集合
     */
    public List<BusOperLog> selectBusOperLogList(BusOperLog busOperLog);

    /**
     * 新增oper_log
     * 
     * @param busOperLog oper_log
     * @return 结果
     */
    public int insertBusOperLog(BusOperLog busOperLog);

    /**
     * 修改oper_log
     * 
     * @param busOperLog oper_log
     * @return 结果
     */
    public int updateBusOperLog(BusOperLog busOperLog);

    /**
     * 批量删除oper_log
     * 
     * @param ids 需要删除的oper_log主键集合
     * @return 结果
     */
    public int deleteBusOperLogByIds(Long[] ids);

    /**
     * 删除oper_log信息
     * 
     * @param id oper_log主键
     * @return 结果
     */
    public int deleteBusOperLogById(Long id);
}
