package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsLockFactor;

/**
 * 锁信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IPdsLockFactorService 
{
    /**
     * 查询锁信息
     * 
     * @param id 锁信息主键
     * @return 锁信息
     */
    public PdsLockFactor selectPdsLockFactorById(Long id);

    /**
     * 查询锁信息列表
     * 
     * @param pdsLockFactor 锁信息
     * @return 锁信息集合
     */
    public List<PdsLockFactor> selectPdsLockFactorList(PdsLockFactor pdsLockFactor);

    /**
     * 新增锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    public int insertPdsLockFactor(PdsLockFactor pdsLockFactor);

    /**
     * 修改锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    public int updatePdsLockFactor(PdsLockFactor pdsLockFactor);

    /**
     * 批量删除锁信息
     * 
     * @param ids 需要删除的锁信息主键集合
     * @return 结果
     */
    public int deletePdsLockFactorByIds(Long[] ids);

    /**
     * 删除锁信息信息
     * 
     * @param id 锁信息主键
     * @return 结果
     */
    public int deletePdsLockFactorById(Long id);
}
