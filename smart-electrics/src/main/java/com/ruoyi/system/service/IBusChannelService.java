package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusChannel;

/**
 * 管道Service接口
 * 
 * @author yk
 * @date 2024-04-26
 */
public interface IBusChannelService 
{
    /**
     * 查询管道
     * 
     * @param id 管道主键
     * @return 管道
     */
    public BusChannel selectBusChannelById(String id);

    /**
     * 查询管道列表
     * 
     * @param busChannel 管道
     * @return 管道集合
     */
    public List<BusChannel> selectBusChannelList(BusChannel busChannel);

    /**
     * 新增管道
     * 
     * @param busChannel 管道
     * @return 结果
     */
    public int insertBusChannel(BusChannel busChannel);

    /**
     * 修改管道
     * 
     * @param busChannel 管道
     * @return 结果
     */
    public int updateBusChannel(BusChannel busChannel);

    /**
     * 批量删除管道
     * 
     * @param ids 需要删除的管道主键集合
     * @return 结果
     */
    public int deleteBusChannelByIds(String[] ids);

    /**
     * 删除管道信息
     * 
     * @param id 管道主键
     * @return 结果
     */
    public int deleteBusChannelById(String id);
}
