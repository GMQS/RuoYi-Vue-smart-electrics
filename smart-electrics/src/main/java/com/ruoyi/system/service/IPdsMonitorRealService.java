package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsMonitorReal;

/**
 * 实时监控数据Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface IPdsMonitorRealService 
{
    /**
     * 查询实时监控数据
     * 
     * @param id 实时监控数据主键
     * @return 实时监控数据
     */
    public PdsMonitorReal selectPdsMonitorRealById(Long id);

    /**
     * 查询实时监控数据列表
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 实时监控数据集合
     */
    public List<PdsMonitorReal> selectPdsMonitorRealList(PdsMonitorReal pdsMonitorReal);

    /**
     * 新增实时监控数据
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 结果
     */
    public int insertPdsMonitorReal(PdsMonitorReal pdsMonitorReal);

    /**
     * 修改实时监控数据
     * 
     * @param pdsMonitorReal 实时监控数据
     * @return 结果
     */
    public int updatePdsMonitorReal(PdsMonitorReal pdsMonitorReal);

    /**
     * 批量删除实时监控数据
     * 
     * @param ids 需要删除的实时监控数据主键集合
     * @return 结果
     */
    public int deletePdsMonitorRealByIds(Long[] ids);

    /**
     * 删除实时监控数据信息
     * 
     * @param id 实时监控数据主键
     * @return 结果
     */
    public int deletePdsMonitorRealById(Long id);
}
