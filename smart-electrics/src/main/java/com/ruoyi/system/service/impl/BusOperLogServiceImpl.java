package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusOperLogMapper;
import com.ruoyi.system.domain.BusOperLog;
import com.ruoyi.system.service.IBusOperLogService;

/**
 * oper_logService业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@Service
public class BusOperLogServiceImpl implements IBusOperLogService 
{
    @Autowired
    private BusOperLogMapper busOperLogMapper;

    /**
     * 查询oper_log
     * 
     * @param id oper_log主键
     * @return oper_log
     */
    @Override
    public BusOperLog selectBusOperLogById(Long id)
    {
        return busOperLogMapper.selectBusOperLogById(id);
    }

    /**
     * 查询oper_log列表
     * 
     * @param busOperLog oper_log
     * @return oper_log
     */
    @Override
    public List<BusOperLog> selectBusOperLogList(BusOperLog busOperLog)
    {
        return busOperLogMapper.selectBusOperLogList(busOperLog);
    }

    /**
     * 新增oper_log
     * 
     * @param busOperLog oper_log
     * @return 结果
     */
    @Override
    public int insertBusOperLog(BusOperLog busOperLog)
    {
        return busOperLogMapper.insertBusOperLog(busOperLog);
    }

    /**
     * 修改oper_log
     * 
     * @param busOperLog oper_log
     * @return 结果
     */
    @Override
    public int updateBusOperLog(BusOperLog busOperLog)
    {
        return busOperLogMapper.updateBusOperLog(busOperLog);
    }

    /**
     * 批量删除oper_log
     * 
     * @param ids 需要删除的oper_log主键
     * @return 结果
     */
    @Override
    public int deleteBusOperLogByIds(Long[] ids)
    {
        return busOperLogMapper.deleteBusOperLogByIds(ids);
    }

    /**
     * 删除oper_log信息
     * 
     * @param id oper_log主键
     * @return 结果
     */
    @Override
    public int deleteBusOperLogById(Long id)
    {
        return busOperLogMapper.deleteBusOperLogById(id);
    }
}
