package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LocationManageMapper;
import com.ruoyi.system.domain.LocationManage;
import com.ruoyi.system.service.ILocationManageService;

/**
 * location_manageService业务层处理
 * 
 * @author yk
 * @date 2024-03-07
 */
@Service
public class LocationManageServiceImpl implements ILocationManageService 
{
    @Autowired
    private LocationManageMapper locationManageMapper;

    /**
     * 查询location_manage
     * 
     * @param id location_manage主键
     * @return location_manage
     */
    @Override
    public LocationManage selectLocationManageById(Long id)
    {
        return locationManageMapper.selectLocationManageById(id);
    }

    /**
     * 查询location_manage列表
     * 
     * @param locationManage location_manage
     * @return location_manage
     */
    @Override
    public List<LocationManage> selectLocationManageList(LocationManage locationManage)
    {
        return locationManageMapper.selectLocationManageList(locationManage);
    }

    /**
     * 新增location_manage
     * 
     * @param locationManage location_manage
     * @return 结果
     */
    @Override
    public int insertLocationManage(LocationManage locationManage)
    {
        locationManage.setCreateTime(DateUtils.getNowDate());
        locationManage.setCreateBy(SecurityUtils.getUsername());
        return locationManageMapper.insertLocationManage(locationManage);
    }

    /**
     * 修改location_manage
     * 
     * @param locationManage location_manage
     * @return 结果
     */
    @Override
    public int updateLocationManage(LocationManage locationManage)
    {
        locationManage.setCreateBy(SecurityUtils.getUsername());
        return locationManageMapper.updateLocationManage(locationManage);
    }

    /**
     * 批量删除location_manage
     * 
     * @param ids 需要删除的location_manage主键
     * @return 结果
     */
    @Override
    public int deleteLocationManageByIds(Long[] ids)
    {
        return locationManageMapper.deleteLocationManageByIds(ids);
    }

    /**
     * 删除location_manage信息
     * 
     * @param id location_manage主键
     * @return 结果
     */
    @Override
    public int deleteLocationManageById(Long id)
    {
        return locationManageMapper.deleteLocationManageById(id);
    }
}
