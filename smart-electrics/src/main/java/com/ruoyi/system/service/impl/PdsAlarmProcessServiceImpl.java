package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsAlarmProcessMapper;
import com.ruoyi.system.domain.PdsAlarmProcess;
import com.ruoyi.system.service.IPdsAlarmProcessService;

/**
 * 告警处理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Service
public class PdsAlarmProcessServiceImpl implements IPdsAlarmProcessService 
{
    @Autowired
    private PdsAlarmProcessMapper pdsAlarmProcessMapper;

    /**
     * 查询告警处理
     * 
     * @param id 告警处理主键
     * @return 告警处理
     */
    @Override
    public PdsAlarmProcess selectPdsAlarmProcessById(Long id)
    {
        return pdsAlarmProcessMapper.selectPdsAlarmProcessById(id);
    }

    /**
     * 查询告警处理列表
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 告警处理
     */
    @Override
    public List<PdsAlarmProcess> selectPdsAlarmProcessList(PdsAlarmProcess pdsAlarmProcess)
    {
        return pdsAlarmProcessMapper.selectPdsAlarmProcessList(pdsAlarmProcess);
    }

    /**
     * 新增告警处理
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 结果
     */
    @Override
    public int insertPdsAlarmProcess(PdsAlarmProcess pdsAlarmProcess)
    {
        return pdsAlarmProcessMapper.insertPdsAlarmProcess(pdsAlarmProcess);
    }

    /**
     * 修改告警处理
     * 
     * @param pdsAlarmProcess 告警处理
     * @return 结果
     */
    @Override
    public int updatePdsAlarmProcess(PdsAlarmProcess pdsAlarmProcess)
    {
        return pdsAlarmProcessMapper.updatePdsAlarmProcess(pdsAlarmProcess);
    }

    /**
     * 批量删除告警处理
     * 
     * @param ids 需要删除的告警处理主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmProcessByIds(Long[] ids)
    {
        return pdsAlarmProcessMapper.deletePdsAlarmProcessByIds(ids);
    }

    /**
     * 删除告警处理信息
     * 
     * @param id 告警处理主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmProcessById(Long id)
    {
        return pdsAlarmProcessMapper.deletePdsAlarmProcessById(id);
    }
}
