package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CfgEmailMapper;
import com.ruoyi.system.domain.CfgEmail;
import com.ruoyi.system.service.ICfgEmailService;

/**
 * 告警配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@Service
public class CfgEmailServiceImpl implements ICfgEmailService 
{
    @Autowired
    private CfgEmailMapper cfgEmailMapper;

    /**
     * 查询告警配置
     * 
     * @param id 告警配置主键
     * @return 告警配置
     */
    @Override
    public CfgEmail selectCfgEmailById(Long id)
    {
        return cfgEmailMapper.selectCfgEmailById(id);
    }

    /**
     * 查询告警配置列表
     * 
     * @param cfgEmail 告警配置
     * @return 告警配置
     */
    @Override
    public List<CfgEmail> selectCfgEmailList(CfgEmail cfgEmail)
    {
        return cfgEmailMapper.selectCfgEmailList(cfgEmail);
    }

    /**
     * 新增告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    @Override
    public int insertCfgEmail(CfgEmail cfgEmail)
    {
        return cfgEmailMapper.insertCfgEmail(cfgEmail);
    }

    /**
     * 修改告警配置
     * 
     * @param cfgEmail 告警配置
     * @return 结果
     */
    @Override
    public int updateCfgEmail(CfgEmail cfgEmail)
    {
        return cfgEmailMapper.updateCfgEmail(cfgEmail);
    }

    /**
     * 批量删除告警配置
     * 
     * @param ids 需要删除的告警配置主键
     * @return 结果
     */
    @Override
    public int deleteCfgEmailByIds(Long[] ids)
    {
        return cfgEmailMapper.deleteCfgEmailByIds(ids);
    }

    /**
     * 删除告警配置信息
     * 
     * @param id 告警配置主键
     * @return 结果
     */
    @Override
    public int deleteCfgEmailById(Long id)
    {
        return cfgEmailMapper.deleteCfgEmailById(id);
    }
}
