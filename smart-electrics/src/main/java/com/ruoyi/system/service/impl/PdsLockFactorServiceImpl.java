package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsLockFactorMapper;
import com.ruoyi.system.domain.PdsLockFactor;
import com.ruoyi.system.service.IPdsLockFactorService;

/**
 * 锁信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsLockFactorServiceImpl implements IPdsLockFactorService 
{
    @Autowired
    private PdsLockFactorMapper pdsLockFactorMapper;

    /**
     * 查询锁信息
     * 
     * @param id 锁信息主键
     * @return 锁信息
     */
    @Override
    public PdsLockFactor selectPdsLockFactorById(Long id)
    {
        return pdsLockFactorMapper.selectPdsLockFactorById(id);
    }

    /**
     * 查询锁信息列表
     * 
     * @param pdsLockFactor 锁信息
     * @return 锁信息
     */
    @Override
    public List<PdsLockFactor> selectPdsLockFactorList(PdsLockFactor pdsLockFactor)
    {
        return pdsLockFactorMapper.selectPdsLockFactorList(pdsLockFactor);
    }

    /**
     * 新增锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    @Override
    public int insertPdsLockFactor(PdsLockFactor pdsLockFactor)
    {
        return pdsLockFactorMapper.insertPdsLockFactor(pdsLockFactor);
    }

    /**
     * 修改锁信息
     * 
     * @param pdsLockFactor 锁信息
     * @return 结果
     */
    @Override
    public int updatePdsLockFactor(PdsLockFactor pdsLockFactor)
    {
        return pdsLockFactorMapper.updatePdsLockFactor(pdsLockFactor);
    }

    /**
     * 批量删除锁信息
     * 
     * @param ids 需要删除的锁信息主键
     * @return 结果
     */
    @Override
    public int deletePdsLockFactorByIds(Long[] ids)
    {
        return pdsLockFactorMapper.deletePdsLockFactorByIds(ids);
    }

    /**
     * 删除锁信息信息
     * 
     * @param id 锁信息主键
     * @return 结果
     */
    @Override
    public int deletePdsLockFactorById(Long id)
    {
        return pdsLockFactorMapper.deletePdsLockFactorById(id);
    }
}
