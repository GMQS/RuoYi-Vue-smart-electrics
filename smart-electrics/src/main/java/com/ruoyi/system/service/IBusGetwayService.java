package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusGetway;

/**
 * 网关
Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IBusGetwayService 
{
    /**
     * 查询网关

     * 
     * @param id 网关
主键
     * @return 网关

     */
    public BusGetway selectBusGetwayById(String id);

    /**
     * 查询网关
列表
     * 
     * @param busGetway 网关

     * @return 网关
集合
     */
    public List<BusGetway> selectBusGetwayList(BusGetway busGetway);

    /**
     * 新增网关

     * 
     * @param busGetway 网关

     * @return 结果
     */
    public int insertBusGetway(BusGetway busGetway);

    /**
     * 修改网关

     * 
     * @param busGetway 网关

     * @return 结果
     */
    public int updateBusGetway(BusGetway busGetway);

    /**
     * 批量删除网关

     * 
     * @param ids 需要删除的网关
主键集合
     * @return 结果
     */
    public int deleteBusGetwayByIds(String[] ids);

    /**
     * 删除网关
信息
     * 
     * @param id 网关
主键
     * @return 结果
     */
    public int deleteBusGetwayById(String id);
}
