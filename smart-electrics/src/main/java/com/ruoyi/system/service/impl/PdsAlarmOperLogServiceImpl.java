package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsAlarmOperLogMapper;
import com.ruoyi.system.domain.PdsAlarmOperLog;
import com.ruoyi.system.service.IPdsAlarmOperLogService;

/**
 * 告警操作日志Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Service
public class PdsAlarmOperLogServiceImpl implements IPdsAlarmOperLogService 
{
    @Autowired
    private PdsAlarmOperLogMapper pdsAlarmOperLogMapper;

    /**
     * 查询告警操作日志
     * 
     * @param id 告警操作日志主键
     * @return 告警操作日志
     */
    @Override
    public PdsAlarmOperLog selectPdsAlarmOperLogById(Long id)
    {
        return pdsAlarmOperLogMapper.selectPdsAlarmOperLogById(id);
    }

    /**
     * 查询告警操作日志列表
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 告警操作日志
     */
    @Override
    public List<PdsAlarmOperLog> selectPdsAlarmOperLogList(PdsAlarmOperLog pdsAlarmOperLog)
    {
        return pdsAlarmOperLogMapper.selectPdsAlarmOperLogList(pdsAlarmOperLog);
    }

    /**
     * 新增告警操作日志
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 结果
     */
    @Override
    public int insertPdsAlarmOperLog(PdsAlarmOperLog pdsAlarmOperLog)
    {
        int ret = pdsAlarmOperLogMapper.insertPdsAlarmOperLog(pdsAlarmOperLog);
        return ret;
    }

    /**
     * 修改告警操作日志
     * 
     * @param pdsAlarmOperLog 告警操作日志
     * @return 结果
     */
    @Override
    public int updatePdsAlarmOperLog(PdsAlarmOperLog pdsAlarmOperLog)
    {
        return pdsAlarmOperLogMapper.updatePdsAlarmOperLog(pdsAlarmOperLog);
    }

    /**
     * 批量删除告警操作日志
     * 
     * @param ids 需要删除的告警操作日志主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmOperLogByIds(Long[] ids)
    {
        return pdsAlarmOperLogMapper.deletePdsAlarmOperLogByIds(ids);
    }

    /**
     * 删除告警操作日志信息
     * 
     * @param id 告警操作日志主键
     * @return 结果
     */
    @Override
    public int deletePdsAlarmOperLogById(Long id)
    {
        return pdsAlarmOperLogMapper.deletePdsAlarmOperLogById(id);
    }
}
