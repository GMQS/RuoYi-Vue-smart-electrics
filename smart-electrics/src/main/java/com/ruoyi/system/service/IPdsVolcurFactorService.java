package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsVolcurFactor;

/**
 * 电压电流信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IPdsVolcurFactorService 
{
    /**
     * 查询电压电流信息
     * 
     * @param id 电压电流信息主键
     * @return 电压电流信息
     */
    public PdsVolcurFactor selectPdsVolcurFactorById(Long id);

    /**
     * 查询电压电流信息列表
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 电压电流信息集合
     */
    public List<PdsVolcurFactor> selectPdsVolcurFactorList(PdsVolcurFactor pdsVolcurFactor);

    /**
     * 新增电压电流信息
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 结果
     */
    public int insertPdsVolcurFactor(PdsVolcurFactor pdsVolcurFactor);

    /**
     * 修改电压电流信息
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 结果
     */
    public int updatePdsVolcurFactor(PdsVolcurFactor pdsVolcurFactor);

    /**
     * 批量删除电压电流信息
     * 
     * @param ids 需要删除的电压电流信息主键集合
     * @return 结果
     */
    public int deletePdsVolcurFactorByIds(Long[] ids);

    /**
     * 删除电压电流信息信息
     * 
     * @param id 电压电流信息主键
     * @return 结果
     */
    public int deletePdsVolcurFactorById(Long id);
}
