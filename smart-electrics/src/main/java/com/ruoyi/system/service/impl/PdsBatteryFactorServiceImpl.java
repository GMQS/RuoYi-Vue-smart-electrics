package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsBatteryFactorMapper;
import com.ruoyi.system.domain.PdsBatteryFactor;
import com.ruoyi.system.service.IPdsBatteryFactorService;

/**
 * 电池信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsBatteryFactorServiceImpl implements IPdsBatteryFactorService 
{
    @Autowired
    private PdsBatteryFactorMapper pdsBatteryFactorMapper;

    /**
     * 查询电池信息
     * 
     * @param id 电池信息主键
     * @return 电池信息
     */
    @Override
    public PdsBatteryFactor selectPdsBatteryFactorById(Long id)
    {
        return pdsBatteryFactorMapper.selectPdsBatteryFactorById(id);
    }

    /**
     * 查询电池信息列表
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 电池信息
     */
    @Override
    public List<PdsBatteryFactor> selectPdsBatteryFactorList(PdsBatteryFactor pdsBatteryFactor)
    {
        return pdsBatteryFactorMapper.selectPdsBatteryFactorList(pdsBatteryFactor);
    }

    /**
     * 新增电池信息
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 结果
     */
    @Override
    public int insertPdsBatteryFactor(PdsBatteryFactor pdsBatteryFactor)
    {
        return pdsBatteryFactorMapper.insertPdsBatteryFactor(pdsBatteryFactor);
    }

    /**
     * 修改电池信息
     * 
     * @param pdsBatteryFactor 电池信息
     * @return 结果
     */
    @Override
    public int updatePdsBatteryFactor(PdsBatteryFactor pdsBatteryFactor)
    {
        return pdsBatteryFactorMapper.updatePdsBatteryFactor(pdsBatteryFactor);
    }

    /**
     * 批量删除电池信息
     * 
     * @param ids 需要删除的电池信息主键
     * @return 结果
     */
    @Override
    public int deletePdsBatteryFactorByIds(Long[] ids)
    {
        return pdsBatteryFactorMapper.deletePdsBatteryFactorByIds(ids);
    }

    /**
     * 删除电池信息信息
     * 
     * @param id 电池信息主键
     * @return 结果
     */
    @Override
    public int deletePdsBatteryFactorById(Long id)
    {
        return pdsBatteryFactorMapper.deletePdsBatteryFactorById(id);
    }
}
