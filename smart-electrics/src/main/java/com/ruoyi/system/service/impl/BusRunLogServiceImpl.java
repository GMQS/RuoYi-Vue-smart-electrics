package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusRunLogMapper;
import com.ruoyi.system.domain.BusRunLog;
import com.ruoyi.system.service.IBusRunLogService;

/**
 * run_logService业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@Service
public class BusRunLogServiceImpl implements IBusRunLogService 
{
    @Autowired
    private BusRunLogMapper busRunLogMapper;

    /**
     * 查询run_log
     * 
     * @param id run_log主键
     * @return run_log
     */
    @Override
    public BusRunLog selectBusRunLogById(Long id)
    {
        return busRunLogMapper.selectBusRunLogById(id);
    }

    /**
     * 查询run_log列表
     * 
     * @param busRunLog run_log
     * @return run_log
     */
    @Override
    public List<BusRunLog> selectBusRunLogList(BusRunLog busRunLog)
    {
        return busRunLogMapper.selectBusRunLogList(busRunLog);
    }

    /**
     * 新增run_log
     * 
     * @param busRunLog run_log
     * @return 结果
     */
    @Override
    public int insertBusRunLog(BusRunLog busRunLog)
    {
        return busRunLogMapper.insertBusRunLog(busRunLog);
    }

    /**
     * 修改run_log
     * 
     * @param busRunLog run_log
     * @return 结果
     */
    @Override
    public int updateBusRunLog(BusRunLog busRunLog)
    {
        return busRunLogMapper.updateBusRunLog(busRunLog);
    }

    /**
     * 批量删除run_log
     * 
     * @param ids 需要删除的run_log主键
     * @return 结果
     */
    @Override
    public int deleteBusRunLogByIds(Long[] ids)
    {
        return busRunLogMapper.deleteBusRunLogByIds(ids);
    }

    /**
     * 删除run_log信息
     * 
     * @param id run_log主键
     * @return 结果
     */
    @Override
    public int deleteBusRunLogById(Long id)
    {
        return busRunLogMapper.deleteBusRunLogById(id);
    }
}
