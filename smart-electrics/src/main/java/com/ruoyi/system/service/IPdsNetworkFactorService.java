package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsNetworkFactor;

/**
 * 网路信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IPdsNetworkFactorService 
{
    /**
     * 查询网路信息
     * 
     * @param id 网路信息主键
     * @return 网路信息
     */
    public PdsNetworkFactor selectPdsNetworkFactorById(Long id);

    /**
     * 查询网路信息列表
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 网路信息集合
     */
    public List<PdsNetworkFactor> selectPdsNetworkFactorList(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 新增网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    public int insertPdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 修改网路信息
     * 
     * @param pdsNetworkFactor 网路信息
     * @return 结果
     */
    public int updatePdsNetworkFactor(PdsNetworkFactor pdsNetworkFactor);

    /**
     * 批量删除网路信息
     * 
     * @param ids 需要删除的网路信息主键集合
     * @return 结果
     */
    public int deletePdsNetworkFactorByIds(Long[] ids);

    /**
     * 删除网路信息信息
     * 
     * @param id 网路信息主键
     * @return 结果
     */
    public int deletePdsNetworkFactorById(Long id);
}
