package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusChannelMapper;
import com.ruoyi.system.domain.BusChannel;
import com.ruoyi.system.service.IBusChannelService;

/**
 * 管道Service业务层处理
 * 
 * @author yk
 * @date 2024-04-26
 */
@Service
public class BusChannelServiceImpl implements IBusChannelService 
{
    @Autowired
    private BusChannelMapper busChannelMapper;

    /**
     * 查询管道
     * 
     * @param id 管道主键
     * @return 管道
     */
    @Override
    public BusChannel selectBusChannelById(String id)
    {
        return busChannelMapper.selectBusChannelById(id);
    }

    /**
     * 查询管道列表
     * 
     * @param busChannel 管道
     * @return 管道
     */
    @Override
    public List<BusChannel> selectBusChannelList(BusChannel busChannel)
    {
        return busChannelMapper.selectBusChannelList(busChannel);
    }

    /**
     * 新增管道
     * 
     * @param busChannel 管道
     * @return 结果
     */
    @Override
    public int insertBusChannel(BusChannel busChannel)
    {
        busChannel.setCreateTime(DateUtils.getNowDate());
        busChannel.setCreateBy(SecurityUtils.getUsername());
        return busChannelMapper.insertBusChannel(busChannel);
    }

    /**
     * 修改管道
     * 
     * @param busChannel 管道
     * @return 结果
     */
    @Override
    public int updateBusChannel(BusChannel busChannel)
    {
        busChannel.setUpdateTime(DateUtils.getNowDate());
        busChannel.setUpdateBy(SecurityUtils.getUsername());
        return busChannelMapper.updateBusChannel(busChannel);
    }

    /**
     * 批量删除管道
     * 
     * @param ids 需要删除的管道主键
     * @return 结果
     */
    @Override
    public int deleteBusChannelByIds(String[] ids)
    {
        return busChannelMapper.deleteBusChannelByIds(ids);
    }

    /**
     * 删除管道信息
     * 
     * @param id 管道主键
     * @return 结果
     */
    @Override
    public int deleteBusChannelById(String id)
    {
        return busChannelMapper.deleteBusChannelById(id);
    }
}
