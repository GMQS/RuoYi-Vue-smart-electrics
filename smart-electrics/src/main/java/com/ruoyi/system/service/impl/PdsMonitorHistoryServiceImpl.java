package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsMonitorHistoryMapper;
import com.ruoyi.system.domain.PdsMonitorHistory;
import com.ruoyi.system.service.IPdsMonitorHistoryService;

/**
 * 历史监控数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class PdsMonitorHistoryServiceImpl implements IPdsMonitorHistoryService 
{
    @Autowired
    private PdsMonitorHistoryMapper pdsMonitorHistoryMapper;

    /**
     * 查询历史监控数据
     * 
     * @param id 历史监控数据主键
     * @return 历史监控数据
     */
    @Override
    public PdsMonitorHistory selectPdsMonitorHistoryById(Long id)
    {
        return pdsMonitorHistoryMapper.selectPdsMonitorHistoryById(id);
    }

    /**
     * 查询历史监控数据列表
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 历史监控数据
     */
    @Override
    public List<PdsMonitorHistory> selectPdsMonitorHistoryList(PdsMonitorHistory pdsMonitorHistory)
    {
        return pdsMonitorHistoryMapper.selectPdsMonitorHistoryList(pdsMonitorHistory);
    }

    /**
     * 新增历史监控数据
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 结果
     */
    @Override
    public int insertPdsMonitorHistory(PdsMonitorHistory pdsMonitorHistory)
    {
        return pdsMonitorHistoryMapper.insertPdsMonitorHistory(pdsMonitorHistory);
    }

    /**
     * 修改历史监控数据
     * 
     * @param pdsMonitorHistory 历史监控数据
     * @return 结果
     */
    @Override
    public int updatePdsMonitorHistory(PdsMonitorHistory pdsMonitorHistory)
    {
        return pdsMonitorHistoryMapper.updatePdsMonitorHistory(pdsMonitorHistory);
    }

    /**
     * 批量删除历史监控数据
     * 
     * @param ids 需要删除的历史监控数据主键
     * @return 结果
     */
    @Override
    public int deletePdsMonitorHistoryByIds(Long[] ids)
    {
        return pdsMonitorHistoryMapper.deletePdsMonitorHistoryByIds(ids);
    }

    /**
     * 删除历史监控数据信息
     * 
     * @param id 历史监控数据主键
     * @return 结果
     */
    @Override
    public int deletePdsMonitorHistoryById(Long id)
    {
        return pdsMonitorHistoryMapper.deletePdsMonitorHistoryById(id);
    }
}
