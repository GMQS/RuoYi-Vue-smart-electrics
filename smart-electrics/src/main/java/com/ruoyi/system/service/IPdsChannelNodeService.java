package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PdsChannelNode;

/**
 * 管道节点关联Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IPdsChannelNodeService 
{
    /**
     * 查询管道节点关联
     * 
     * @param id 管道节点关联主键
     * @return 管道节点关联
     */
    public PdsChannelNode selectPdsChannelNodeById(Long id);

    /**
     * 查询管道下所有节点
     *
     * @param channelId 管道id
     * @return 管道节点关联集合
     */
    public List<PdsChannelNode> selectPdsChannelNodeListByChannelId(Long channelId);

    /**
     * 查询管道节点关联列表
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 管道节点关联集合
     */
    public List<PdsChannelNode> selectPdsChannelNodeList(PdsChannelNode pdsChannelNode);

    /**
     * 新增管道节点关联
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 结果
     */
    public int insertPdsChannelNode(PdsChannelNode pdsChannelNode);

    /**
     * 修改管道节点关联
     * 
     * @param pdsChannelNode 管道节点关联
     * @return 结果
     */
    public int updatePdsChannelNode(PdsChannelNode pdsChannelNode);

    /**
     * 批量删除管道节点关联
     * 
     * @param ids 需要删除的管道节点关联主键集合
     * @return 结果
     */
    public int deletePdsChannelNodeByIds(Long[] ids);

    /**
     * 删除管道节点关联信息
     * 
     * @param id 管道节点关联主键
     * @return 结果
     */
    public int deletePdsChannelNodeById(Long id);


    /**
     * 根据管道id和节点id删除管道节点关联
     *
     * @return 结果
     */
    public int deleteByChannelIdAndNodeId(PdsChannelNode pdsChannelNode);
}
