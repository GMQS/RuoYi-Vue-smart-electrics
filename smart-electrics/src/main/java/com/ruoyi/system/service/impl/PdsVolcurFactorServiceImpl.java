package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PdsVolcurFactorMapper;
import com.ruoyi.system.domain.PdsVolcurFactor;
import com.ruoyi.system.service.IPdsVolcurFactorService;

/**
 * 电压电流信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class PdsVolcurFactorServiceImpl implements IPdsVolcurFactorService 
{
    @Autowired
    private PdsVolcurFactorMapper pdsVolcurFactorMapper;

    /**
     * 查询电压电流信息
     * 
     * @param id 电压电流信息主键
     * @return 电压电流信息
     */
    @Override
    public PdsVolcurFactor selectPdsVolcurFactorById(Long id)
    {
        return pdsVolcurFactorMapper.selectPdsVolcurFactorById(id);
    }

    /**
     * 查询电压电流信息列表
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 电压电流信息
     */
    @Override
    public List<PdsVolcurFactor> selectPdsVolcurFactorList(PdsVolcurFactor pdsVolcurFactor)
    {
        return pdsVolcurFactorMapper.selectPdsVolcurFactorList(pdsVolcurFactor);
    }

    /**
     * 新增电压电流信息
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 结果
     */
    @Override
    public int insertPdsVolcurFactor(PdsVolcurFactor pdsVolcurFactor)
    {
        return pdsVolcurFactorMapper.insertPdsVolcurFactor(pdsVolcurFactor);
    }

    /**
     * 修改电压电流信息
     * 
     * @param pdsVolcurFactor 电压电流信息
     * @return 结果
     */
    @Override
    public int updatePdsVolcurFactor(PdsVolcurFactor pdsVolcurFactor)
    {
        return pdsVolcurFactorMapper.updatePdsVolcurFactor(pdsVolcurFactor);
    }

    /**
     * 批量删除电压电流信息
     * 
     * @param ids 需要删除的电压电流信息主键
     * @return 结果
     */
    @Override
    public int deletePdsVolcurFactorByIds(Long[] ids)
    {
        return pdsVolcurFactorMapper.deletePdsVolcurFactorByIds(ids);
    }

    /**
     * 删除电压电流信息信息
     * 
     * @param id 电压电流信息主键
     * @return 结果
     */
    @Override
    public int deletePdsVolcurFactorById(Long id)
    {
        return pdsVolcurFactorMapper.deletePdsVolcurFactorById(id);
    }
}
