package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * run_log对象 bus_run_log
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
public class BusRunLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date runTime;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private String runType;

    /** 运行内容 */
    @Excel(name = "运行内容")
    private String runInfo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRunTime(Date runTime) 
    {
        this.runTime = runTime;
    }

    public Date getRunTime() 
    {
        return runTime;
    }
    public void setRunType(String runType) 
    {
        this.runType = runType;
    }

    public String getRunType() 
    {
        return runType;
    }
    public void setRunInfo(String runInfo) 
    {
        this.runInfo = runInfo;
    }

    public String getRunInfo() 
    {
        return runInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("runTime", getRunTime())
            .append("runType", getRunType())
            .append("runInfo", getRunInfo())
            .toString();
    }
}
