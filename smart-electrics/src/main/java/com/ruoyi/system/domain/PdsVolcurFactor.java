package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 电压电流信息对象 pds_volcur_factor
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public class PdsVolcurFactor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String nodeName;

    /** 电压监测启动 */
    @Excel(name = "电压监测启动")
    private Boolean volMonEnable;

    /** 电流检测启动 */
    @Excel(name = "电流检测启动")
    private Boolean curMonEnable;

    /** 数据监测周期 */
    @Excel(name = "数据监测周期")
    private Long monitorInterval;

    /** 汇报周期 */
    @Excel(name = "汇报周期")
    private Long reportInterval;

    /** 电压告警上限 */
    @Excel(name = "电压告警上限")
    private Long volUpperLimit;

    /** 电压告警下限 */
    @Excel(name = "电压告警下限")
    private Long volLowerLimit;

    /** 电流告警上限 */
    @Excel(name = "电流告警上限")
    private Long curUpperLimit;

    /** 电流告警下限 */
    @Excel(name = "电流告警下限")
    private Long curLowerLimit;

    /** 告警延迟 */
    @Excel(name = "告警延迟")
    private Long alarmDelay;

    /** 告警汇报间隔 */
    @Excel(name = "告警汇报间隔")
    private Long alarmInterval;

    /** 短信告警 */
    @Excel(name = "短信告警")
    private Boolean alarmMsg;

    /** 邮件告警 */
    @Excel(name = "邮件告警")
    private Boolean alarmEmail;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNodeName(String nodeName) 
    {
        this.nodeName = nodeName;
    }

    public String getNodeName() 
    {
        return nodeName;
    }
    public void setVolMonEnable(Boolean volMonEnable) 
    {
        this.volMonEnable = volMonEnable;
    }

    public Boolean getVolMonEnable() 
    {
        return volMonEnable;
    }
    public void setCurMonEnable(Boolean curMonEnable) 
    {
        this.curMonEnable = curMonEnable;
    }

    public Boolean getCurMonEnable() 
    {
        return curMonEnable;
    }
    public void setMonitorInterval(Long monitorInterval) 
    {
        this.monitorInterval = monitorInterval;
    }

    public Long getMonitorInterval() 
    {
        return monitorInterval;
    }
    public void setReportInterval(Long reportInterval) 
    {
        this.reportInterval = reportInterval;
    }

    public Long getReportInterval() 
    {
        return reportInterval;
    }
    public void setVolUpperLimit(Long volUpperLimit) 
    {
        this.volUpperLimit = volUpperLimit;
    }

    public Long getVolUpperLimit() 
    {
        return volUpperLimit;
    }
    public void setVolLowerLimit(Long volLowerLimit) 
    {
        this.volLowerLimit = volLowerLimit;
    }

    public Long getVolLowerLimit() 
    {
        return volLowerLimit;
    }
    public void setCurUpperLimit(Long curUpperLimit) 
    {
        this.curUpperLimit = curUpperLimit;
    }

    public Long getCurUpperLimit() 
    {
        return curUpperLimit;
    }
    public void setCurLowerLimit(Long curLowerLimit) 
    {
        this.curLowerLimit = curLowerLimit;
    }

    public Long getCurLowerLimit() 
    {
        return curLowerLimit;
    }
    public void setAlarmDelay(Long alarmDelay) 
    {
        this.alarmDelay = alarmDelay;
    }

    public Long getAlarmDelay() 
    {
        return alarmDelay;
    }
    public void setAlarmInterval(Long alarmInterval) 
    {
        this.alarmInterval = alarmInterval;
    }

    public Long getAlarmInterval() 
    {
        return alarmInterval;
    }
    public void setAlarmMsg(Boolean alarmMsg) 
    {
        this.alarmMsg = alarmMsg;
    }

    public Boolean getAlarmMsg() 
    {
        return alarmMsg;
    }
    public void setAlarmEmail(Boolean alarmEmail) 
    {
        this.alarmEmail = alarmEmail;
    }

    public Boolean getAlarmEmail() 
    {
        return alarmEmail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("nodeName", getNodeName())
            .append("remark", getRemark())
            .append("volMonEnable", getVolMonEnable())
            .append("curMonEnable", getCurMonEnable())
            .append("monitorInterval", getMonitorInterval())
            .append("reportInterval", getReportInterval())
            .append("volUpperLimit", getVolUpperLimit())
            .append("volLowerLimit", getVolLowerLimit())
            .append("curUpperLimit", getCurUpperLimit())
            .append("curLowerLimit", getCurLowerLimit())
            .append("alarmDelay", getAlarmDelay())
            .append("alarmInterval", getAlarmInterval())
            .append("alarmMsg", getAlarmMsg())
            .append("alarmEmail", getAlarmEmail())
            .toString();
    }
}
