package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实时监控数据对象 pds_monitor_real
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class PdsMonitorReal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 配电箱名称 */
    @Excel(name = "配电箱名称")
    private String busName;

    /** 管道名称 */
    @Excel(name = "管道名称")
    private String channelName;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String factorName;

    /** 监测类型 */
    @Excel(name = "监测类型")
    private String monitorType;

    /** 监测内容 */
    @Excel(name = "监测内容")
    private String monitorContent;

    /** 质量戳 */
    @Excel(name = "质量戳")
    private String quality;

    /** 时间戳 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间戳", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date monitorTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setBusName(String busName) 
    {
        this.busName = busName;
    }

    public String getBusName() 
    {
        return busName;
    }
    public void setChannelName(String channelName) 
    {
        this.channelName = channelName;
    }

    public String getChannelName() 
    {
        return channelName;
    }
    public void setFactorName(String factorName) 
    {
        this.factorName = factorName;
    }

    public String getFactorName() 
    {
        return factorName;
    }
    public void setMonitorType(String monitorType) 
    {
        this.monitorType = monitorType;
    }

    public String getMonitorType() 
    {
        return monitorType;
    }
    public void setMonitorContent(String monitorContent) 
    {
        this.monitorContent = monitorContent;
    }

    public String getMonitorContent() 
    {
        return monitorContent;
    }
    public void setQuality(String quality) 
    {
        this.quality = quality;
    }

    public String getQuality() 
    {
        return quality;
    }
    public void setMonitorTime(Date monitorTime) 
    {
        this.monitorTime = monitorTime;
    }

    public Date getMonitorTime() 
    {
        return monitorTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectName", getProjectName())
            .append("busName", getBusName())
            .append("channelName", getChannelName())
            .append("factorName", getFactorName())
            .append("monitorType", getMonitorType())
            .append("monitorContent", getMonitorContent())
            .append("quality", getQuality())
            .append("monitorTime", getMonitorTime())
            .append("remark", getRemark())
            .toString();
    }
}
