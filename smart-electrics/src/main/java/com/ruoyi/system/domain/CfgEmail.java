package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 告警配置对象 cfg_email
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public class CfgEmail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 电话 */
    @Excel(name = "电话")
    private String mobile;

    /** 邮箱服务器 */
    @Excel(name = "邮箱服务器")
    private String smtpServer;

    /** 邮箱服务器端口 */
    @Excel(name = "邮箱服务器端口")
    private Long smtpPort;

    /** 接收人地址 */
    @Excel(name = "接收人地址")
    private String receiverAddress;

    /** 接收人姓名 */
    @Excel(name = "接收人姓名")
    private String receiverName;

    /** 邮件主题 */
    @Excel(name = "邮件主题")
    private String title;

    /** 短信id */
    @Excel(name = "短信id")
    private Long cfgMsgId;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 告警间隔 */
    @Excel(name = "告警间隔")
    private Long interval;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setSmtpServer(String smtpServer) 
    {
        this.smtpServer = smtpServer;
    }

    public String getSmtpServer() 
    {
        return smtpServer;
    }
    public void setSmtpPort(Long smtpPort) 
    {
        this.smtpPort = smtpPort;
    }

    public Long getSmtpPort() 
    {
        return smtpPort;
    }
    public void setReceiverAddress(String receiverAddress) 
    {
        this.receiverAddress = receiverAddress;
    }

    public String getReceiverAddress() 
    {
        return receiverAddress;
    }
    public void setReceiverName(String receiverName) 
    {
        this.receiverName = receiverName;
    }

    public String getReceiverName() 
    {
        return receiverName;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setCfgMsgId(Long cfgMsgId) 
    {
        this.cfgMsgId = cfgMsgId;
    }

    public Long getCfgMsgId() 
    {
        return cfgMsgId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setInterval(Long interval) 
    {
        this.interval = interval;
    }

    public Long getInterval() 
    {
        return interval;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectId", getProjectId())
            .append("mobile", getMobile())
            .append("smtpServer", getSmtpServer())
            .append("smtpPort", getSmtpPort())
            .append("receiverAddress", getReceiverAddress())
            .append("receiverName", getReceiverName())
            .append("title", getTitle())
            .append("cfgMsgId", getCfgMsgId())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("interval", getInterval())
            .toString();
    }
}
