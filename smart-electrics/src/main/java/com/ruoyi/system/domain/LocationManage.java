package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * location_manage对象 location_manage
 * 
 * @author yk
 * @date 2024-03-07
 */
public class LocationManage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Excel(name = "id")
    private Long id;

    /**
     * 归属区编号
     */
    @Excel(name = "归属区编号")
    private Long locationId;

    /**
     * 归属区
     */
    @Excel(name = "归属区")
    private String locationName;

    /**
     * 经纬度位置
     */
    @Excel(name = "经度位置")
    private String latitude;
    @Excel(name = "纬度位置")
    private String longitude;

    private Long projectId;

    @Excel(name = "备注")
    private String remark;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getLocationId() {
        return locationId;
    }


    public Long getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Long projectId){this.projectId=projectId;}

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("locationId", getLocationId())
                .append("locationName", getLocationName())
                .append("latitude", getLatitude())
                .append("longitude", getLongitude())
                .append("remark",getRemark())
                .append("projectId", getProjectId())
                .append("createTime", getCreateTime())
                .append("createBy",getCreateBy())
                .toString();
    }
}
