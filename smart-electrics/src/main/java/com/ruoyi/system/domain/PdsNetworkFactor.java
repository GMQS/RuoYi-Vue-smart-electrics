package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 网路信息对象 pds_network_factor
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public class PdsNetworkFactor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String nodeName;

    /** 通信协议 */
    @Excel(name = "通信协议")
    private String protocol;

    /** 设备IP地址 */
    @Excel(name = "设备IP地址")
    private String ip;

    /** 丢包率监测 */
    @Excel(name = "丢包率监测")
    private Boolean perMonitor;

    /** 网络流量监测 */
    @Excel(name = "网络流量监测")
    private Boolean netflowMonitor;

    /** 数据监测周期 */
    @Excel(name = "数据监测周期")
    private Long monitorInterval;

    /** 数据汇报周期 */
    @Excel(name = "数据汇报周期")
    private Long reportInterval;

    /** 丢包率告警上限 */
    @Excel(name = "丢包率告警上限")
    private Long perUpperLimit;

    /** 告警延迟 */
    @Excel(name = "告警延迟")
    private Long alarmDelay;

    /** 告警汇报间隔 */
    @Excel(name = "告警汇报间隔")
    private Long alarmInterval;

    /** 短信告警 */
    @Excel(name = "短信告警")
    private Boolean alarmMsg;

    /** 邮箱告警 */
    @Excel(name = "邮箱告警")
    private Boolean alarmEmail;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNodeName(String nodeName) 
    {
        this.nodeName = nodeName;
    }

    public String getNodeName() 
    {
        return nodeName;
    }
    public void setProtocol(String protocol) 
    {
        this.protocol = protocol;
    }

    public String getProtocol() 
    {
        return protocol;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setPerMonitor(Boolean perMonitor) 
    {
        this.perMonitor = perMonitor;
    }

    public Boolean getPerMonitor() 
    {
        return perMonitor;
    }
    public void setNetflowMonitor(Boolean netflowMonitor) 
    {
        this.netflowMonitor = netflowMonitor;
    }

    public Boolean getNetflowMonitor() 
    {
        return netflowMonitor;
    }
    public void setMonitorInterval(Long monitorInterval) 
    {
        this.monitorInterval = monitorInterval;
    }

    public Long getMonitorInterval() 
    {
        return monitorInterval;
    }
    public void setReportInterval(Long reportInterval) 
    {
        this.reportInterval = reportInterval;
    }

    public Long getReportInterval() 
    {
        return reportInterval;
    }
    public void setPerUpperLimit(Long perUpperLimit) 
    {
        this.perUpperLimit = perUpperLimit;
    }

    public Long getPerUpperLimit() 
    {
        return perUpperLimit;
    }
    public void setAlarmDelay(Long alarmDelay) 
    {
        this.alarmDelay = alarmDelay;
    }

    public Long getAlarmDelay() 
    {
        return alarmDelay;
    }
    public void setAlarmInterval(Long alarmInterval) 
    {
        this.alarmInterval = alarmInterval;
    }

    public Long getAlarmInterval() 
    {
        return alarmInterval;
    }
    public void setAlarmMsg(Boolean alarmMsg) 
    {
        this.alarmMsg = alarmMsg;
    }

    public Boolean getAlarmMsg() 
    {
        return alarmMsg;
    }
    public void setAlarmEmail(Boolean alarmEmail) 
    {
        this.alarmEmail = alarmEmail;
    }

    public Boolean getAlarmEmail() 
    {
        return alarmEmail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("nodeName", getNodeName())
            .append("protocol", getProtocol())
            .append("ip", getIp())
            .append("remark", getRemark())
            .append("perMonitor", getPerMonitor())
            .append("netflowMonitor", getNetflowMonitor())
            .append("monitorInterval", getMonitorInterval())
            .append("reportInterval", getReportInterval())
            .append("perUpperLimit", getPerUpperLimit())
            .append("alarmDelay", getAlarmDelay())
            .append("alarmInterval", getAlarmInterval())
            .append("alarmMsg", getAlarmMsg())
            .append("alarmEmail", getAlarmEmail())
            .toString();
    }
}
