package com.ruoyi.system.domain.idList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ProjectIdInfo {
    private String projectName;

    private Long projectId;


    public void setprojectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getprojectId() {
        return projectId;
    }

    public void setprojectName(String projectName) {
        this.projectName = projectName;
    }

    public String getprojectName() {
        return projectName;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("projectId", getprojectId())
                .append("projectName", getprojectName())
                .toString();
    }

}
