package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 网关
对象 bus_getway
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public class BusGetway extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 编码 */
    @Excel(name = "编码")
    private String code;

    /** 配电箱id */
    @Excel(name = "配电箱id")
    private Long deviceId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 地址码 */
    @Excel(name = "地址码")
    private String address;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String ipCode;

    /** 端口 */
    @Excel(name = "端口")
    private Long port;

    /** 离线间隔时间 */
    @Excel(name = "离线间隔时间")
    private Long outTime;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setDeviceId(Long deviceId) 
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId() 
    {
        return deviceId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setIpCode(String ipCode) 
    {
        this.ipCode = ipCode;
    }

    public String getIpCode() 
    {
        return ipCode;
    }
    public void setPort(Long port) 
    {
        this.port = port;
    }

    public Long getPort() 
    {
        return port;
    }
    public void setOutTime(Long outTime) 
    {
        this.outTime = outTime;
    }

    public Long getOutTime() 
    {
        return outTime;
    }
    public void setDeleted(Integer deleted) 
    {
        this.deleted = deleted;
    }

    public Integer getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("deviceId", getDeviceId())
            .append("name", getName())
            .append("address", getAddress())
            .append("ipCode", getIpCode())
            .append("port", getPort())
            .append("outTime", getOutTime())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
