package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 告警信息对象 pds_alarm_info
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class PdsAlarmInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 编号(雪花ID) */
    @Excel(name = "编号(雪花ID)")
    private String alarmInfoId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 配电箱名称 */
    @Excel(name = "配电箱名称")
    private String busName;

    /** 管道名称 */
    @Excel(name = "管道名称")
    private String channelName;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String factorName;

    /** 告警内容 */
    @Excel(name = "告警内容")
    private String alarmContent;

    /** 告警时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "告警时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date alarmTime;

    /** 告警等级(级别) */
    @Excel(name = "告警等级(级别)")
    private String alarmRank;

    /** 告警状态(未、中、恢复、忽略) */
    @Excel(name = "告警状态(未、中、恢复、忽略)")
    private String alarmState;

    /** 告警处理id */
    @Excel(name = "告警处理id")
    private Long alarmProcessId;

    /** 上次报警时间  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上次报警时间 ", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastAlarmTime;

    /** 重复报警次数 */
    @Excel(name = "重复报警次数")
    private Long alarmCount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAlarmInfoId(String alarmInfoId) 
    {
        this.alarmInfoId = alarmInfoId;
    }

    public String getAlarmInfoId() 
    {
        return alarmInfoId;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setBusName(String busName) 
    {
        this.busName = busName;
    }

    public String getBusName() 
    {
        return busName;
    }
    public void setChannelName(String channelName) 
    {
        this.channelName = channelName;
    }

    public String getChannelName() 
    {
        return channelName;
    }
    public void setFactorName(String factorName) 
    {
        this.factorName = factorName;
    }

    public String getFactorName() 
    {
        return factorName;
    }
    public void setAlarmContent(String alarmContent) 
    {
        this.alarmContent = alarmContent;
    }

    public String getAlarmContent() 
    {
        return alarmContent;
    }
    public void setAlarmTime(Date alarmTime) 
    {
        this.alarmTime = alarmTime;
    }

    public Date getAlarmTime() 
    {
        return alarmTime;
    }
    public void setAlarmRank(String alarmRank) 
    {
        this.alarmRank = alarmRank;
    }

    public String getAlarmRank() 
    {
        return alarmRank;
    }
    public void setAlarmState(String alarmState) 
    {
        this.alarmState = alarmState;
    }

    public String getAlarmState() 
    {
        return alarmState;
    }
    public void setAlarmProcessId(Long alarmProcessId) 
    {
        this.alarmProcessId = alarmProcessId;
    }

    public Long getAlarmProcessId() 
    {
        return alarmProcessId;
    }
    public void setLastAlarmTime(Date lastAlarmTime) 
    {
        this.lastAlarmTime = lastAlarmTime;
    }

    public Date getLastAlarmTime() 
    {
        return lastAlarmTime;
    }
    public void setAlarmCount(Long alarmCount) 
    {
        this.alarmCount = alarmCount;
    }

    public Long getAlarmCount() 
    {
        return alarmCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("alarmInfoId", getAlarmInfoId())
            .append("projectName", getProjectName())
            .append("busName", getBusName())
            .append("channelName", getChannelName())
            .append("factorName", getFactorName())
            .append("alarmContent", getAlarmContent())
            .append("alarmTime", getAlarmTime())
            .append("alarmRank", getAlarmRank())
            .append("alarmState", getAlarmState())
            .append("alarmProcessId", getAlarmProcessId())
            .append("lastAlarmTime", getLastAlarmTime())
            .append("alarmCount", getAlarmCount())
            .toString();
    }
}
