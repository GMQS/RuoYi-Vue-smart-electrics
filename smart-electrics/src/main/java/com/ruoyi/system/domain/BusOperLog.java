package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * oper_log对象 bus_oper_log
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
public class BusOperLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operTime;

    /** 用户 */
    @Excel(name = "用户")
    private Long operUserId;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String operIp;

    /** 类型 */
    @Excel(name = "类型")
    private String operType;

    /** 操作内容 */
    @Excel(name = "操作内容")
    private String operInfo;

    /** 具体操作 */
    @Excel(name = "具体操作")
    private String operSpecInfo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOperTime(Date operTime) 
    {
        this.operTime = operTime;
    }

    public Date getOperTime() 
    {
        return operTime;
    }
    public void setOperUserId(Long operUserId) 
    {
        this.operUserId = operUserId;
    }

    public Long getOperUserId() 
    {
        return operUserId;
    }
    public void setOperIp(String operIp) 
    {
        this.operIp = operIp;
    }

    public String getOperIp() 
    {
        return operIp;
    }
    public void setOperType(String operType) 
    {
        this.operType = operType;
    }

    public String getOperType() 
    {
        return operType;
    }
    public void setOperInfo(String operInfo) 
    {
        this.operInfo = operInfo;
    }

    public String getOperInfo() 
    {
        return operInfo;
    }
    public void setOperSpecInfo(String operSpecInfo) 
    {
        this.operSpecInfo = operSpecInfo;
    }

    public String getOperSpecInfo() 
    {
        return operSpecInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("operTime", getOperTime())
            .append("operUserId", getOperUserId())
            .append("operIp", getOperIp())
            .append("operType", getOperType())
            .append("operInfo", getOperInfo())
            .append("operSpecInfo", getOperSpecInfo())
            .toString();
    }
}
