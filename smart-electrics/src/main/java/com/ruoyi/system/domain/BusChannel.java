package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 管道对象 bus_channel
 * 
 * @author yk
 * @date 2024-04-26
 */
public class BusChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 通道编号 */
    @Excel(name = "通道编号")
    private String code;

    /** 通道名称 */
    @Excel(name = "通道名称")
    private String name;

    /** 网关json */
    @Excel(name = "网关json")
    private String getwayJson;

    /** 超时时间时长 */
    @Excel(name = "超时时间时长")
    private Long upTime;

    /** 程序集名称 */
    @Excel(name = "程序集名称")
    private String assembly;

    /** 父级id */
    @Excel(name = "父级id")
    private Long parentId;

    /** 是否删除 */
    private Integer deleted;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGetwayJson(String getwayJson) 
    {
        this.getwayJson = getwayJson;
    }

    public String getGetwayJson() 
    {
        return getwayJson;
    }
    public void setUpTime(Long upTime) 
    {
        this.upTime = upTime;
    }

    public Long getUpTime() 
    {
        return upTime;
    }
    public void setAssembly(String assembly) 
    {
        this.assembly = assembly;
    }

    public String getAssembly() 
    {
        return assembly;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setDeleted(Integer deleted) 
    {
        this.deleted = deleted;
    }

    public Integer getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("getwayJson", getGetwayJson())
            .append("upTime", getUpTime())
            .append("assembly", getAssembly())
            .append("parentId", getParentId())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
