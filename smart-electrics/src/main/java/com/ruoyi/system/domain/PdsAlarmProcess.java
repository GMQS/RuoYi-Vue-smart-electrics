package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 告警处理对象 pds_alarm_process
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public class PdsAlarmProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 处理人 */
    @Excel(name = "处理人")
    private String processPerson;

    /** 处理类型 */
    @Excel(name = "处理类型")
    private String processType;

    /** 处理状态 */
    @Excel(name = "处理状态")
    private String processState;

    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "处理时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date processTime;

    /** 处理描述 */
    @Excel(name = "处理描述")
    private String processDescribe;

    /** 计划完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planDate;

    /** 完成(恢复)时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成(恢复)时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishDate;

    /** 恢复类型 */
    @Excel(name = "恢复类型")
    private String finishType;

    /** 确认人 */
    @Excel(name = "确认人")
    private String checkPerson;

    /** 确认时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "确认时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProcessPerson(String processPerson) 
    {
        this.processPerson = processPerson;
    }

    public String getProcessPerson() 
    {
        return processPerson;
    }
    public void setProcessType(String processType) 
    {
        this.processType = processType;
    }

    public String getProcessType() 
    {
        return processType;
    }
    public void setProcessState(String processState) 
    {
        this.processState = processState;
    }

    public String getProcessState() 
    {
        return processState;
    }
    public void setProcessTime(Date processTime) 
    {
        this.processTime = processTime;
    }

    public Date getProcessTime() 
    {
        return processTime;
    }
    public void setProcessDescribe(String processDescribe) 
    {
        this.processDescribe = processDescribe;
    }

    public String getProcessDescribe() 
    {
        return processDescribe;
    }
    public void setPlanDate(Date planDate) 
    {
        this.planDate = planDate;
    }

    public Date getPlanDate() 
    {
        return planDate;
    }
    public void setFinishDate(Date finishDate) 
    {
        this.finishDate = finishDate;
    }

    public Date getFinishDate() 
    {
        return finishDate;
    }
    public void setFinishType(String finishType) 
    {
        this.finishType = finishType;
    }

    public String getFinishType() 
    {
        return finishType;
    }
    public void setCheckPerson(String checkPerson) 
    {
        this.checkPerson = checkPerson;
    }

    public String getCheckPerson() 
    {
        return checkPerson;
    }
    public void setCheckTime(Date checkTime) 
    {
        this.checkTime = checkTime;
    }

    public Date getCheckTime() 
    {
        return checkTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("processPerson", getProcessPerson())
            .append("processType", getProcessType())
            .append("processState", getProcessState())
            .append("processTime", getProcessTime())
            .append("processDescribe", getProcessDescribe())
            .append("planDate", getPlanDate())
            .append("finishDate", getFinishDate())
            .append("finishType", getFinishType())
            .append("checkPerson", getCheckPerson())
            .append("checkTime", getCheckTime())
            .toString();
    }
}
