package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 锁信息对象 pds_lock_factor
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public class PdsLockFactor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String nodeName;

    /** 供应商 */
    @Excel(name = "供应商")
    private String maker;

    /** 出厂编号 */
    @Excel(name = "出厂编号")
    private String factoryNumber;

    /** 锁类型 */
    @Excel(name = "锁类型")
    private String lockType;

    /** 锁启用 */
    @Excel(name = "锁启用")
    private Boolean deviceEnable;

    /** 监测启动 */
    @Excel(name = "监测启动")
    private Boolean monitorEnable;

    /** 数据监测周期 */
    @Excel(name = "数据监测周期")
    private Long monitorInterval;

    /** 监测汇报周期 */
    @Excel(name = "监测汇报周期")
    private Long reportInterval;

    /** 失败次数上限 */
    @Excel(name = "失败次数上限")
    private Long failUpperLimit;

    /** 告警延迟 */
    @Excel(name = "告警延迟")
    private Long alarmDelay;

    /** 告警汇报间隔 */
    @Excel(name = "告警汇报间隔")
    private Long alarmInterval;

    /** 离线告警 */
    @Excel(name = "离线告警")
    private Boolean offlineAlarm;

    /** 短信告警 */
    @Excel(name = "短信告警")
    private Boolean alarmMsg;

    /** 邮箱告警 */
    @Excel(name = "邮箱告警")
    private Boolean alarmEmail;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNodeName(String nodeName) 
    {
        this.nodeName = nodeName;
    }

    public String getNodeName() 
    {
        return nodeName;
    }
    public void setMaker(String maker) 
    {
        this.maker = maker;
    }

    public String getMaker() 
    {
        return maker;
    }
    public void setFactoryNumber(String factoryNumber) 
    {
        this.factoryNumber = factoryNumber;
    }

    public String getFactoryNumber() 
    {
        return factoryNumber;
    }
    public void setLockType(String lockType) 
    {
        this.lockType = lockType;
    }

    public String getLockType() 
    {
        return lockType;
    }
    public void setDeviceEnable(Boolean deviceEnable) 
    {
        this.deviceEnable = deviceEnable;
    }

    public Boolean getDeviceEnable() 
    {
        return deviceEnable;
    }
    public void setMonitorEnable(Boolean monitorEnable) 
    {
        this.monitorEnable = monitorEnable;
    }

    public Boolean getMonitorEnable() 
    {
        return monitorEnable;
    }
    public void setMonitorInterval(Long monitorInterval) 
    {
        this.monitorInterval = monitorInterval;
    }

    public Long getMonitorInterval() 
    {
        return monitorInterval;
    }
    public void setReportInterval(Long reportInterval) 
    {
        this.reportInterval = reportInterval;
    }

    public Long getReportInterval() 
    {
        return reportInterval;
    }
    public void setFailUpperLimit(Long failUpperLimit) 
    {
        this.failUpperLimit = failUpperLimit;
    }

    public Long getFailUpperLimit() 
    {
        return failUpperLimit;
    }
    public void setAlarmDelay(Long alarmDelay) 
    {
        this.alarmDelay = alarmDelay;
    }

    public Long getAlarmDelay() 
    {
        return alarmDelay;
    }
    public void setAlarmInterval(Long alarmInterval) 
    {
        this.alarmInterval = alarmInterval;
    }

    public Long getAlarmInterval() 
    {
        return alarmInterval;
    }
    public void setOfflineAlarm(Boolean offlineAlarm) 
    {
        this.offlineAlarm = offlineAlarm;
    }

    public Boolean getOfflineAlarm() 
    {
        return offlineAlarm;
    }
    public void setAlarmMsg(Boolean alarmMsg) 
    {
        this.alarmMsg = alarmMsg;
    }

    public Boolean getAlarmMsg() 
    {
        return alarmMsg;
    }
    public void setAlarmEmail(Boolean alarmEmail) 
    {
        this.alarmEmail = alarmEmail;
    }

    public Boolean getAlarmEmail() 
    {
        return alarmEmail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("nodeName", getNodeName())
            .append("maker", getMaker())
            .append("factoryNumber", getFactoryNumber())
            .append("lockType", getLockType())
            .append("remark", getRemark())
            .append("deviceEnable", getDeviceEnable())
            .append("monitorEnable", getMonitorEnable())
            .append("monitorInterval", getMonitorInterval())
            .append("reportInterval", getReportInterval())
            .append("failUpperLimit", getFailUpperLimit())
            .append("alarmDelay", getAlarmDelay())
            .append("alarmInterval", getAlarmInterval())
            .append("offlineAlarm", getOfflineAlarm())
            .append("alarmMsg", getAlarmMsg())
            .append("alarmEmail", getAlarmEmail())
            .toString();
    }
}
