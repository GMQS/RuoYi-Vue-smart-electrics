package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 定时任务日志对象 schedule_job_log
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
public class ScheduleJobLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @Excel(name = "")
    private Long jobId;

    /** spring bean名称 */
    @Excel(name = "spring bean名称")
    private String beanName;

    /** 参数 */
    @Excel(name = "参数")
    private String configParams;

    /** 任务状态  0：成功    1：失败 */
    @Excel(name = "任务状态  0：成功    1：失败")
    private Long status;

    /** 失败信息 */
    @Excel(name = "失败信息")
    private String error;

    /** 耗时(单位：毫秒) */
    @Excel(name = "耗时(单位：毫秒)")
    private Long times;

    /** 归属者(到时用于数据权限控制) */
    @Excel(name = "归属者(到时用于数据权限控制)")
    private Long ownerUserId;

    /** 归属部门(到时用于数据权限控制) */
    @Excel(name = "归属部门(到时用于数据权限控制)")
    private Long ownerDeptId;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createUserId;

    /** 修改者 */
    @Excel(name = "修改者")
    private Long updateUserId;

    /** 删除标志(0正常，1删除) */
    @Excel(name = "删除标志(0正常，1删除)")
    private Integer hasDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobId(Long jobId) 
    {
        this.jobId = jobId;
    }

    public Long getJobId() 
    {
        return jobId;
    }
    public void setBeanName(String beanName) 
    {
        this.beanName = beanName;
    }

    public String getBeanName() 
    {
        return beanName;
    }
    public void setConfigParams(String configParams) 
    {
        this.configParams = configParams;
    }

    public String getConfigParams() 
    {
        return configParams;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setError(String error) 
    {
        this.error = error;
    }

    public String getError() 
    {
        return error;
    }
    public void setTimes(Long times) 
    {
        this.times = times;
    }

    public Long getTimes() 
    {
        return times;
    }
    public void setOwnerUserId(Long ownerUserId) 
    {
        this.ownerUserId = ownerUserId;
    }

    public Long getOwnerUserId() 
    {
        return ownerUserId;
    }
    public void setOwnerDeptId(Long ownerDeptId) 
    {
        this.ownerDeptId = ownerDeptId;
    }

    public Long getOwnerDeptId() 
    {
        return ownerDeptId;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }
    public void setHasDeleted(Integer hasDeleted) 
    {
        this.hasDeleted = hasDeleted;
    }

    public Integer getHasDeleted() 
    {
        return hasDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobId", getJobId())
            .append("beanName", getBeanName())
            .append("configParams", getConfigParams())
            .append("status", getStatus())
            .append("error", getError())
            .append("times", getTimes())
            .append("remark", getRemark())
            .append("ownerUserId", getOwnerUserId())
            .append("ownerDeptId", getOwnerDeptId())
            .append("createUserId", getCreateUserId())
            .append("updateUserId", getUpdateUserId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("hasDeleted", getHasDeleted())
            .toString();
    }
}
