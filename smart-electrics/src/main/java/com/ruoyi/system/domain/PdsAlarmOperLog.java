package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 告警操作日志对象 pds_alarm_oper_log
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public class PdsAlarmOperLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 操作id */
    private Long id;

    /** 告警记录编号 */
    @Excel(name = "告警记录编号")
    private Long alarmId;

    /** 操作员名称 */
    @Excel(name = "操作员名称")
    private String operUserName;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private Long operType;

    /** 操作结果 */
    @Excel(name = "操作结果")
    private String operResult;

    /** 访问地址 */
    @Excel(name = "访问地址")
    private String operIp;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAlarmId(Long alarmId) 
    {
        this.alarmId = alarmId;
    }

    public Long getAlarmId() 
    {
        return alarmId;
    }
    public void setOperUserName(String operUserName) 
    {
        this.operUserName = operUserName;
    }

    public String getOperUserName() 
    {
        return operUserName;
    }
    public void setOperType(Long operType) 
    {
        this.operType = operType;
    }

    public Long getOperType() 
    {
        return operType;
    }
    public void setOperResult(String operResult) 
    {
        this.operResult = operResult;
    }

    public String getOperResult() 
    {
        return operResult;
    }
    public void setOperIp(String operIp) 
    {
        this.operIp = operIp;
    }

    public String getOperIp() 
    {
        return operIp;
    }
    public void setOperTime(Date operTime) 
    {
        this.operTime = operTime;
    }

    public Date getOperTime() 
    {
        return operTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("alarmId", getAlarmId())
            .append("operUserName", getOperUserName())
            .append("operType", getOperType())
            .append("operResult", getOperResult())
            .append("operIp", getOperIp())
            .append("operTime", getOperTime())
            .toString();
    }
}
