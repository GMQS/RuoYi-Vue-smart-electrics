package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * host对象 sever_host
 * 
 * @author ruoyi
 * @date 2024-01-22
 */
public class SeverHost extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 服务器主机地址 */
    @Excel(name = "服务器主机地址")
    private String hostIp;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 服务器主机平台 */
    @Excel(name = "服务器主机平台")
    private String hostPlatform;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHostIp(String hostIp) 
    {
        this.hostIp = hostIp;
    }

    public String getHostIp() 
    {
        return hostIp;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setHostPlatform(String hostPlatform) 
    {
        this.hostPlatform = hostPlatform;
    }

    public String getHostPlatform() 
    {
        return hostPlatform;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hostIp", getHostIp())
            .append("status", getStatus())
            .append("hostPlatform", getHostPlatform())
            .toString();
    }
}
