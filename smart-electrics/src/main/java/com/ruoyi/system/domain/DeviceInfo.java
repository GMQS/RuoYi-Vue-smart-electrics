package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 配电箱管理对象 device_info
 * 
 * @author yk
 * @date 2024-04-24
 */
public class DeviceInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 编号 */
    @Excel(name = "编号")
    private String deviceCode;

    /** 名称 */
    @Excel(name = "名称")
    private String deviceName;

    /** 商家 */
    @Excel(name = "商家")
    private String business;

    /** 型号 */
    @Excel(name = "型号")
    private String deviceModel;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date productDate;

    /** 状态 */
    @Excel(name = "状态")
    private String deviceState;

    /** 纬度 */
    @Excel(name = "纬度")
    private String latitude;

    /** 纬度 */
    @Excel(name = "经度")
    private String longtitude;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 项目名 */
    @Excel(name = "项目名")
    private String projectName;

    /** 项目json */
    @Excel(name = "项目json")
    private String projectJson;

    /** 区域id */
    @Excel(name = "区域id")
    private Long areaId;

    /** 区域名 */
    @Excel(name = "区域名")
    private String areaName;

    /** 区域json */
    @Excel(name = "区域json")
    private String areaJson;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDeviceCode(String deviceCode) 
    {
        this.deviceCode = deviceCode;
    }

    public String getDeviceCode() 
    {
        return deviceCode;
    }
    public void setDeviceName(String deviceName) 
    {
        this.deviceName = deviceName;
    }

    public String getDeviceName() 
    {
        return deviceName;
    }
    public void setBusiness(String business) 
    {
        this.business = business;
    }

    public String getBusiness() 
    {
        return business;
    }
    public void setDeviceModel(String deviceModel) 
    {
        this.deviceModel = deviceModel;
    }

    public String getDeviceModel() 
    {
        return deviceModel;
    }
    public void setProductDate(Date productDate) 
    {
        this.productDate = productDate;
    }

    public Date getProductDate() 
    {
        return productDate;
    }
    public void setDeviceState(String deviceState) 
    {
        this.deviceState = deviceState;
    }

    public String getDeviceState() 
    {
        return deviceState;
    }
    public void setLatitude(String latitude) 
    {
        this.latitude = latitude;
    }

    public String getLatitude() 
    {
        return latitude;
    }
    public void setLongtitude(String longtitude) 
    {
        this.longtitude = longtitude;
    }

    public String getLongtitude() 
    {
        return longtitude;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getProjectName()
    {
        return projectName;
    }
    public void setProjectJson(String projectJson) 
    {
        this.projectJson = projectJson;
    }

    public String getProjectJson() 
    {
        return projectJson;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setAreaJson(String areaJson) 
    {
        this.areaJson = areaJson;
    }

    public String getAreaJson() 
    {
        return areaJson;
    }
    public void setDeleted(Integer delete)
    {
        this.deleted = deleted;
    }

    public Integer getDeleted()
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deviceCode", getDeviceCode())
            .append("deviceName", getDeviceName())
            .append("business", getBusiness())
            .append("deviceModel", getDeviceModel())
            .append("productDate", getProductDate())
            .append("deviceState", getDeviceState())
            .append("latitude", getLatitude())
            .append("longtitude", getLongtitude())
            .append("projectId", getProjectId())
            .append("projectName", getProjectName())
            .append("projectJson", getProjectJson())
            .append("areaId", getAreaId())
            .append("areaName", getAreaName())
            .append("areaJson", getAreaJson())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("deleted", getDeleted())
            .toString();
    }
}
