package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 详细告警记录对象 pds_all_alarm_info
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class PdsAllAlarmInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 历史编号(雪花ID) */
    @Excel(name = "历史编号(雪花ID)")
    private Long allAlarmInfoId;

    /** 实时编号(雪花ID) */
    @Excel(name = "实时编号(雪花ID)")
    private Long alarmInfoId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 配电箱名称 */
    @Excel(name = "配电箱名称")
    private String busName;

    /** 管道名称 */
    @Excel(name = "管道名称")
    private String channelName;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String factorName;

    /** 告警内容 */
    @Excel(name = "告警内容")
    private String alarmContent;

    /** 告警时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "告警时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date alarmTime;

    /** 告警等级 */
    @Excel(name = "告警等级")
    private String alarmRank;

    /** 告警类型 */
    @Excel(name = "告警类型")
    private String alarmType;

    /** 告警人 */
    @Excel(name = "告警人")
    private String alarmPerson;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAllAlarmInfoId(Long allAlarmInfoId) 
    {
        this.allAlarmInfoId = allAlarmInfoId;
    }

    public Long getAllAlarmInfoId() 
    {
        return allAlarmInfoId;
    }
    public void setAlarmInfoId(Long alarmInfoId) 
    {
        this.alarmInfoId = alarmInfoId;
    }

    public Long getAlarmInfoId() 
    {
        return alarmInfoId;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setBusName(String busName) 
    {
        this.busName = busName;
    }

    public String getBusName() 
    {
        return busName;
    }
    public void setChannelName(String channelName) 
    {
        this.channelName = channelName;
    }

    public String getChannelName() 
    {
        return channelName;
    }
    public void setFactorName(String factorName) 
    {
        this.factorName = factorName;
    }

    public String getFactorName() 
    {
        return factorName;
    }
    public void setAlarmContent(String alarmContent) 
    {
        this.alarmContent = alarmContent;
    }

    public String getAlarmContent() 
    {
        return alarmContent;
    }
    public void setAlarmTime(Date alarmTime) 
    {
        this.alarmTime = alarmTime;
    }

    public Date getAlarmTime() 
    {
        return alarmTime;
    }
    public void setAlarmRank(String alarmRank) 
    {
        this.alarmRank = alarmRank;
    }

    public String getAlarmRank() 
    {
        return alarmRank;
    }
    public void setAlarmType(String alarmType) 
    {
        this.alarmType = alarmType;
    }

    public String getAlarmType() 
    {
        return alarmType;
    }
    public void setAlarmPerson(String alarmPerson) 
    {
        this.alarmPerson = alarmPerson;
    }

    public String getAlarmPerson() 
    {
        return alarmPerson;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("allAlarmInfoId", getAllAlarmInfoId())
            .append("alarmInfoId", getAlarmInfoId())
            .append("projectName", getProjectName())
            .append("busName", getBusName())
            .append("channelName", getChannelName())
            .append("factorName", getFactorName())
            .append("alarmContent", getAlarmContent())
            .append("alarmTime", getAlarmTime())
            .append("alarmRank", getAlarmRank())
            .append("alarmType", getAlarmType())
            .append("alarmPerson", getAlarmPerson())
            .toString();
    }
}
