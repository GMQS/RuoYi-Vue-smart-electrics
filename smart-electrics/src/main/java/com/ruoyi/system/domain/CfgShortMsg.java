package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * msg对象 cfg_short_msg
 * 
 * @author xyq
 * @date 2024-04-29
 */
public class CfgShortMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 短信主题 */
    @Excel(name = "短信主题")
    private String msgAbstract;

    /** 短信内容 */
    @Excel(name = "短信内容")
    private String msg;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMsgAbstract(String msgAbstract) 
    {
        this.msgAbstract = msgAbstract;
    }

    public String getMsgAbstract() 
    {
        return msgAbstract;
    }
    public void setMsg(String msg) 
    {
        this.msg = msg;
    }

    public String getMsg() 
    {
        return msg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("msgAbstract", getMsgAbstract())
            .append("msg", getMsg())
            .toString();
    }
}
